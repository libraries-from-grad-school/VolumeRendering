/*
 *  DopplerVolume.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 2/3/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <fcntl.h>
#include <err.h>
#include <cmath>
#include "DopplerVolume.h"
#include "Utility.h"
#include <glui.h>
#include <GLUT/glut.h>

// Hand-edited transfer function
#define XFERFUNCTEXSIZE 16
unsigned char xferFunc[16][4] = {
{   0,   0,   0,   0 },	// Transparent black
{   4, 233, 236, 255 },
{   4, 160, 245, 255 },
{   4,   4, 243, 255 },
{   4, 251,   6, 255 },
{   4, 199,   5, 255 },
{   6, 144,  10, 255 },
{ 252, 252,   5, 255 },
{ 231, 192,   5, 255 },
{ 252, 143,   5, 255 },
{ 252,   6,   5, 255 },
{ 213,   4,   5, 255 },
{ 191,   4,   6, 255 },
{ 251,   5, 251, 255 },
{ 155,  88, 202, 255 },
{ 232, 232, 233, 255 }
};

// Ugliness to deal with C++ and callbacks
static DopplerVolume *thisObject;
// Callback that can be used by GLUI
void    Sliders( int id ) { thisObject->sliderCallback( id ); }
void Checkboxes( int id ) { thisObject->checkboxCallback( id ); }

// Constructor and destructor
#pragma mark [Con|De]structor
DopplerVolume::DopplerVolume( const char *fileName ) {
	
	thisObject = this;

	int s, t, p;
	int fd = open( fileName, O_RDONLY, 0 );
	if( fd == -1 ) {
		err(1, "Opening file: %s", fileName);
		exit(1);
	}
	
	read( fd, &width,  sizeof(int) );
	read( fd, &height, sizeof(int) );
	read( fd, &depth,  sizeof(int) );
	
	unsigned char *textureData = (unsigned char*)malloc( sizeof(unsigned char) * width * height * depth * 4 );
	if( textureData <= NULL ) {
		fprintf( stderr, "Unable to allocate memory for texture." );
		exit(1);
	}

	for( p = 0; p < depth; p++ ) {
		for( t = 0; t < height; t++ ) {
			for( s = 0; s < width; s++ ) {
				read( fd, &textureData[ p*(width*height*4) + t*(width*4) + s*4 + 0 ], sizeof(unsigned char) );
				read( fd, &textureData[ p*(width*height*4) + t*(width*4) + s*4 + 1 ], sizeof(unsigned char) );
				read( fd, &textureData[ p*(width*height*4) + t*(width*4) + s*4 + 2 ], sizeof(unsigned char) );
				read( fd, &textureData[ p*(width*height*4) + t*(width*4) + s*4 + 3 ], sizeof(unsigned char) );
			}
		}
	}
	close( fd );
	
	imageData = textureData;
//	imageData = (unsigned char*)malloc( sizeof(unsigned char) * 128 * 128 * 4 * 4 );
//	scaleTexture( width, height, depth, textureData, 128, 128, 4, imageData );
//	
//	width = 128;
//	height = 128;
//	depth = 4;
//	
//	free(textureData);
	
	texturesCurrent = false;
	displayListCurrent = false;
}

DopplerVolume::~DopplerVolume() {
	delete this->imageData;
}

#pragma mark -
#pragma mark Texture Routines
// This function expects outImage to be allocated with the required space
unsigned char scaleHelper( float x, float y, float z, unsigned char image, int width, int height, int depth ) {
	return 0xFF;
}

void DopplerVolume::scaleTexture( int  inWidth, int  inHeight, int  inDepth, unsigned char  *inImage,
				   int outWidth, int outHeight, int outDepth, unsigned char *outImage )
{
	int i, j, k, l;
	float x, y, z, xRatio, yRatio, zRatio, result;
	unsigned char x0, x1, y0, y1, z0, z1;
	
	for( i = 0; i < outDepth; i++ ) {
		for( j = 0; j < outHeight; j++ ) {
			for( k = 0; k < outWidth; k++ ) {
				// Find the index into the origional image for each texel in the old one
				x = k/outWidth	* inWidth;
				y = j/outHeight	* inHeight;
				z = i/outDepth	* inDepth;
				// Get the ratios for the contribution of each of the nearest texel value
				xRatio = 1. - (x - (int)floor( x ) );
				yRatio = 1. - (y - (int)floor( y ) );
				zRatio = 1. - (z - (int)floor( z ) );
				for( l = 0; l < 4; l++ ) {

				// Get the nearest (on each side) texel from each dimension
					x0 = inImage[ (int)floor( x ) ];
					x1 = inImage[ (int) ceil( x ) ];
					y0 = inImage[ (int)floor( y ) ];
					y1 = inImage[ (int) ceil( y ) ];
					z0 = inImage[ (int)floor( z ) ];
					z1 = inImage[ (int) ceil( z ) ];
					
				// Accumulate the X,Y,Z values of 
					result  = (xRatio * x0) + ( (1. - xRatio) * x1);
					result += (yRatio * y0) + ( (1. - yRatio) * y1);
					result += (zRatio * z0) + ( (1. - zRatio) * z1);
					result  = result / 3.;
					
				// Place the resulting value for the texel back into the new image
					outImage[ 0 + k*4 + j*outWidth*4 + i*outWidth*outHeight*4] = (unsigned char)result;
					
				}
			}
		}
	}
}


#pragma mark -
#pragma mark UI and Drawing routines

// Create the GLUI widgets necessary for this model
void DopplerVolume::initUI( GLUI *glui ) {	
	glGenTextures( 1, (GLuint*)textureIDs);
	volumeDisplayList = glGenLists( 1 );
	glEnable( GL_TEXTURE_2D );
}

// Callbacks
void DopplerVolume::draw() {
	// Make sure the textures are loaded
/*	if( !texturesCurrent ) {
		refreshTextures();
	}
	
	// If the display list isn't current, refresh it
	if( !displayListCurrent )
		internalDraw();
	else
		glCallList( volumeDisplayList );
	
}

void DopplerVolume::refreshTextures()
{*/
	CheckGlErrors( "Loading texture" ); 

	// Load the transfer function into a texture
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_1D, textureIDs[0] );
	glTexImage1D( GL_TEXTURE_1D, 0, GL_RGBA, XFERFUNCTEXSIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, xferFunc );
	glTexParameterf( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_1D, GL_REPLACE );
	glEnable( GL_TEXTURE_1D );
	
//	// Load the dataset into a texture
//	glActiveTexture( GL_TEXTURE1 );
//	glBindTexture( GL_TEXTURE_3D, textureIDs[0] );
//	// Texture Modes
//	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
//	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
//	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP );
//	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP );
//	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP );
//	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
//	// Load texture data
//	glTexImage3D( GL_TEXTURE_3D, 0, GL_RGBA, 128, 128, depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
//	glEnable( GL_TEXTURE_3D );

	// Load the dataset into a texture
	glActiveTexture( GL_TEXTURE1 );
	glBindTexture( GL_TEXTURE_2D, textureIDs[0] );
	// Texture Modes	

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);		
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );	

	// Load texture data
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, height, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	
	/*
}

#define X_AXIS 0
#define Y_AXIS 1
#define Z_AXIS 2

void DopplerVolume::internalDraw() {
//	GLdouble modelViewMatrix[16], projectionMatrix[16];
//	GLint viewport[4];
//	float scalar;
//	float x, y, z;
//	GLdouble x1, y1, z1, x2, y2, z2;*/
	int i, j, k, axis, direction;
	
	// Store data into display list
//	glNewList( volumeDisplayList, GL_COMPILE_AND_EXECUTE ); {

	// Save the entering modelview matrix
		glPushMatrix();

	// Scale and translate the modelview to allow further calls to be in model space
		glScalef( .25, .25, .25 );

		glBegin( GL_QUADS ); {
			for( i = 0; i < depth; i++ ) {
				glTexCoord2f(  0.,  0. );
//				glTexCoord3f(  0.,  0., ((float)i)/((float)depth) );
				  glVertex3f( -1., -1., ((float)i)/((float)depth) );

				glTexCoord2f(  0.,  1. );
//				glTexCoord3f(  0.,  1., ((float)i)/((float)depth) );
				  glVertex3f( -1.,  1., ((float)i)/((float)depth) );

				glTexCoord2f(  1.,  1. );
//				glTexCoord3f(  1.,  1., ((float)i)/((float)depth) );
				  glVertex3f(  1.,  1., ((float)i)/((float)depth) );

				glTexCoord2f(  1.,  0. );
//				glTexCoord3f(  1.,  0., ((float)i)/((float)depth) );
				  glVertex3f(  1., -1., ((float)i)/((float)depth) );
			}
		} glEnd();

//		glDisable( GL_TEXTURE_2D );

		glPopMatrix();	

//	} glEndList();
	
	displayListCurrent = true;
}

void DopplerVolume::reset() {
	// Set the range sliders appropriately
	xRange[0] = yRange[0] = zRange[0] = 0;
	xRange[1] = width;
	yRange[1] = height;
	zRange[1] = depth;
	sRange[0] = 0.0;
	sRange[1] = 1.0;
	
	// Set the boolean options to defaults
	points = 1;
	jitter = 0;
	
	// Re-seed the random number generator
	srandom( time(NULL) );
	
	displayListCurrent = false;
	
}

void DopplerVolume::checkboxCallback( int id ) {
	displayListCurrent = false;
}

void DopplerVolume::sliderCallback( int id ) {
//	char str[32];
	
	displayListCurrent = false;
/*	
	switch( id ) {
		case XRANGE:
			sprintf( str, "X Range: %f to %f", xRange[0], xRange[1] );
			xRangeLabel->set_text( str );
			break;
		case YRANGE:
			sprintf( str, "Y Range: %f to %f", yRange[0], yRange[1] );
			yRangeLabel->set_text( str );
			break;
		case ZRANGE:;
			sprintf( str, "Z Range: %f to %f", zRange[0], zRange[1] );
			zRangeLabel->set_text( str );
			break;
			//		case RRANGE:
			//			sprintf( str, "Radial Dist.: %f to %f", rRange[0], rRange[1] );
			//			rRangeLabel->set_text( str );
			//			break;
		case TRANGE:
			sprintf( str, "Scalar Range: %f to %f", sRange[0], sRange[1] );
			sRangeLabel->set_text( str );
			break;
	}
*/
}
