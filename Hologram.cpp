/*
 *  hologram.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 5/7/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

// Project includes
#include "Hologram.h"
#include "Utility.h"
// Framework includes
#include <glui.h>
#include <GLUT/glut.h>
// Stdlib includes
#include <cmath>
#include <iostream>
#include <stdio.h>
using namespace std;

// Ugliness to deal with C++ and callbacks
static Hologram *thisObject;

// Constructor and destructor
#pragma mark [Con|De]structor
Hologram::Hologram( int newWidth, int newHeight, int newDepth ){
	// Load image objects (NSImage)
	
}

Hologram::~Hologram() {
	// Deallocate the images
}

// UI and Drawing routines
#pragma mark -
#pragma mark UI and Drawing routines

// Callback that can be used by GLUI
void Sliders( int id ) { thisObject->sliderCallback( id ); }
void Checkboxes( int id) { thisObject->checkboxCallback( id ); }
void Buttons(int id) { thisObject->buttonCallback( id ); }

// Create the GLUI widgets necessary for this model
void Hologram::initUI( GLUI *glui ) {

// Init some things now that OpenGL is initialized

	// Get OpenGL shader program ID's
	vertexShaderID	= glCreateShader( GL_VERTEX_SHADER );
	fragShaderID	= glCreateShader( GL_FRAGMENT_SHADER );
	xferFuncProgram	= glCreateProgram();
	// Get OpenGL retained mode graphics ID's
	displayList		= glGenLists( 1 );// Request display list ID from OpenGL
	// Set texture parameters
	glEnable (GL_TEXTURE_RECTANGLE_ARB);// Enable texture rectangles
	glGenTextures(2, texID);			// Request texture ID from OpenGL
}

// Callbacks
void Hologram::draw() {	

	if( !shaderCurrent )
		this->loadShaders();
	
	if( !textureCurrent )
		this->refreshTexture();
	
	// Freshen the camera
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	gluLookAt( eye[0], eye[1], eye[2], look[0], look[1], look[2], 0.0, 1.0, 0.0 );
	
	// Draw the surrounding scene
	glBegin( GL_QUADS ); {
		// Forward face
		glVertex3f( -2.0, -2.0,  2.0 );
		glVertex3f(  2.0, -2.0,  2.0 );
		glVertex3f(  2.0,  2.0,  2.0 );
		glVertex3f( -2.0,  2.0,  2.0 );		
		// Right face
		glVertex3f(  2.0, -2.0, -2.0 );
		glVertex3f(  2.0, -2.0,  2.0 );
		glVertex3f(  2.0,  2.0,  2.0 );
		glVertex3f(  2.0,  2.0, -2.0 );		
		// Back face
		glVertex3f( -2.0, -2.0, -2.0 );
		glVertex3f(  2.0, -2.0, -2.0 );
		glVertex3f(  2.0,  2.0, -2.0 );
		glVertex3f( -2.0,  2.0, -2.0 );		
		// Left face
		glVertex3f( -2.0, -2.0, -2.0 );
		glVertex3f( -2.0, -2.0,  2.0 );
		glVertex3f( -2.0,  2.0,  2.0 );
		glVertex3f( -2.0,  2.0, -2.0 );		
		// Top face
		glVertex3f( -2.0,  2.0, -2.0 );
		glVertex3f(  2.0,  2.0, -2.0 );
		glVertex3f(  2.0,  2.0,  2.0 );
		glVertex3f( -2.0,  2.0,  2.0 );		
		// Bottom face
		glVertex3f( -2.0, -2.0, -2.0 );
		glVertex3f(  2.0, -2.0, -2.0 );
		glVertex3f(  2.0, -2.0,  2.0 );
		glVertex3f( -2.0, -2.0,  2.0 );
	} glEnd();
	
	// Enable the shader
	glUseProgram( xferFuncProgram );
	// Draw the hologram quad
	glBegin( GL_QUADS ); {
		glVertex3f( -1.0, -1.0, 0.0 );
		glVertex3f(  1.0, -1.0, 0.0 );
		glVertex3f(  1.0,  1.0, 0.0 );
		glVertex3f( -1.0,  1.0, 0.0 );		
	} glEnd();
	glUseProgram(0);
		
	glPopMatrix();
	
	return;
}

#define BLOCK_SIZE 4096
void Hologram::loadShaders( void ) {
//	const char vertexFileName = "xferFunc.vert"
//	const char fragFileName = "xferFunc.frag"
	GLint status;
	unsigned int readBytes = 0;
	char shaderFileBuffer[4096];
	const GLchar *shaderBuffer[1] = { (GLchar*)shaderFileBuffer };
	
	FILE *fileID;
	
	fileID = fopen( "Project5.vert", "r" ); // Open the file for reading
	if( fileID == NULL ) {
		printf("Error opening file: %s\n");//, vertexFileName);
	} else {
		readBytes = fread( (void *)shaderFileBuffer, 1, BLOCK_SIZE, fileID );
		if( readBytes == BLOCK_SIZE ) {
			if( shaderFileBuffer[BLOCK_SIZE-1] != EOF )
				cout << "Vertex shader too large" << endl;
		} else {
			glShaderSource( vertexShaderID, 1, shaderBuffer, (GLint*)&readBytes );		
		}
		
		fclose( fileID );		
	}
	
	glCompileShader( vertexShaderID );
	glGetShaderiv( vertexShaderID, GL_COMPILE_STATUS, &status );
	if( status != GL_TRUE ) {
		glGetShaderInfoLog( vertexShaderID, BLOCK_SIZE, (GLsizei*)&readBytes, shaderFileBuffer );
		cout << shaderFileBuffer << endl;
	}
	glAttachShader( xferFuncProgram, vertexShaderID );
	
	fileID = fopen( "xferFunc.frag", "r" ); // Open the file for reading
	if( fileID == NULL ) {
		printf("Error opening file: %s\n");//, fragFileName);
	} else {
		readBytes = fread( (void *)shaderFileBuffer, 1, BLOCK_SIZE, fileID );
		if( readBytes == BLOCK_SIZE ) {
			if( shaderFileBuffer[BLOCK_SIZE-1] != EOF )
				cout << "Fragment shader too large" << endl;
		} else {
			glShaderSource( fragShaderID, 1, shaderBuffer, (GLint*)&readBytes );		
		}
		
		fclose( fileID );	
	}	
	
	glCompileShader( fragShaderID );
	glGetShaderiv( fragShaderID, GL_COMPILE_STATUS, &status );
	if( status != GL_TRUE ) {
		glGetShaderInfoLog( fragShaderID, BLOCK_SIZE, (GLsizei*)&readBytes, shaderFileBuffer );
		cout << shaderFileBuffer << endl;
	}
	glAttachShader( xferFuncProgram,   fragShaderID );	
	
	glLinkProgram ( xferFuncProgram );
	glValidateProgram( xferFuncProgram );
	glGetProgramiv( xferFuncProgram, GL_VALIDATE_STATUS, &status );
	if( status != GL_TRUE ) {
		glGetProgramInfoLog( xferFuncProgram, BLOCK_SIZE, (GLsizei*)&readBytes, shaderFileBuffer );
		cout << "Error while validating program:" << endl << shaderFileBuffer << endl;
	}	
	
	shaderCurrent = true;
}

void Hologram::refreshTexture( void ) {
	GLfloat xferFunc[7][4] = {
	{ 0.0, 0.0, 0.0, 0.0 },
	{ 0.0, 0.0, 1.0, 1.0 },
	{ 0.0, 1.0, 1.0, 1.0 },
	{ 0.0, 1.0, 0.0, 1.0 },
	{ 1.0, 1.0, 0.0, 1.0 },
	{ 1.0, 0.0, 0.0, 1.0 },
	{ 0.0, 0.0, 0.0, 0.0 }
	};
	
	// Apple specific extensions to improve performance (breaks shaders?)
	//	glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
	//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_STORAGE_HINT_APPLE, GL_STORAGE_SHARED_APPLE);	
	
	// Choose the texture id to set
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_3D, texID[0] );
	// Load texture data
	glTexImage3D(GL_TEXTURE_3D, 0, GL_LUMINANCE, width, height, depth, 0, GL_LUMINANCE, GL_FLOAT, data );
	// Texture Modes
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
	
	glActiveTexture( GL_TEXTURE1 );
	glBindTexture( GL_TEXTURE_1D, texID[1] );
	// Load transfer func data
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, 7, 1, GL_RGBA, GL_FLOAT, xferFunc );
	// Texture modes
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
	
	textureCurrent = true;
}

void findCurveLoc( int frameNum, float vec[3] ) {
	float cX, dX, cY, dY, cZ, dZ;
	float t, framesThisInterval;
	keyframe  *currKeyFrame = &Frames[0];
	
		// Find the keyframes that "most recently passed" for the eye
	while( (currKeyFrame+1)->frame < frameNum && (currKeyFrame+1)->frame != -1 )
		currKeyFrame++;
	
	if( (currKeyFrame+1)->frame == (-1) ) {
		framesThisInterval = (float)(Frames[0].frame - currKeyFrame->frame);
	} else {
		framesThisInterval = (float)((currKeyFrame+1)->frame - currKeyFrame->frame);
	}
	
	t = (float)( frameNum - currKeyFrame->frame ) / framesThisInterval;
	
	cX = -3.0 * currKeyFrame->x + 3.0 * (currKeyFrame+1)->x - 2.0 * currKeyFrame->intervals[1].dxdt - (currKeyFrame+1)->intervals[0].dxdt;
	cY = -3.0 * currKeyFrame->y + 3.0 * (currKeyFrame+1)->y - 2.0 * currKeyFrame->intervals[1].dydt - (currKeyFrame+1)->intervals[0].dydt;
	cZ = -3.0 * currKeyFrame->z + 3.0 * (currKeyFrame+1)->z - 2.0 * currKeyFrame->intervals[1].dzdt - (currKeyFrame+1)->intervals[0].dzdt;
	dX =  2.0 * currKeyFrame->x - 2.0 * (currKeyFrame+1)->x + currKeyFrame->intervals[1].dxdt + (currKeyFrame+1)->intervals[0].dxdt;
	dY =  2.0 * currKeyFrame->y - 2.0 * (currKeyFrame+1)->y + currKeyFrame->intervals[1].dydt + (currKeyFrame+1)->intervals[0].dydt;
	dZ =  2.0 * currKeyFrame->z - 2.0 * (currKeyFrame+1)->z + currKeyFrame->intervals[1].dzdt + (currKeyFrame+1)->intervals[0].dzdt;
	
	vec[0] = currKeyFrame->x + t * (currKeyFrame->intervals[1].dxdt + t * ( cX + t * dX ) );
	vec[1] = currKeyFrame->y + t * (currKeyFrame->intervals[1].dydt + t * ( cY + t * dY ) );
	vec[2] = currKeyFrame->z + t * (currKeyFrame->intervals[1].dzdt + t * ( cZ + t * dZ ) );
	
}

void Hologram::internalDraw() {
	GLuint texLoc;
	float u, v, w;
	int i, j, k;
	
	glNewList( volumeDisplayList, GL_COMPILE_AND_EXECUTE );
	
	// Assume that the shader program is loaded and 'used'
	texLoc = glGetUniformLocation( xferFuncProgram, "scalars" );
	glUniform1i( texLoc, 0 );
	texLoc = glGetUniformLocation( xferFuncProgram, "xferFunc" );
	glUniform1i( texLoc, 1 );
	texLoc = glGetUniformLocation( xferFuncProgram, "filter" );
	glUniform1i( texLoc, GL_TRUE );
	
	glPointSize( 3.0 );
	
	// Point cloud
	glBegin( GL_POINTS );
	for( i = 0; i < depth / 2; i++ ) {
		for( j = 0; j < height / 2; j++ ) {
			for( k = 0; k < width / 2; k++ ) {
				// Re-calculate the u,v,w values with a random offset
				u = modelSpaceCoord( float(k) + randomOffset(), width / 2,  BOUNDS_LOW, BOUNDS_HIGH );
				v = modelSpaceCoord( float(j) + randomOffset(), height / 2, BOUNDS_LOW, BOUNDS_HIGH );
				w = modelSpaceCoord( float(i) + randomOffset(), depth / 2,  BOUNDS_LOW, BOUNDS_HIGH );
				
				// Place the point
				//glTexCoord3f( (u + 1) / 2, (v + 1) / 2, (w + 1) / 2 );	
				glVertex3f( u, v, w );
			}
		}
	}
	glEnd();
	
	texLoc = glGetUniformLocation( xferFuncProgram, "filter" );
	glUniform1i( texLoc, GL_FALSE );
	// Turn-off the shader
	//	glUseProgram( 0 );	
	// Hologram spheres
	if( spheres ) {
		for( i = 0; Frames[i].frame != -1; i++ ) {
			glPushMatrix();
			glTranslatef( Frames[i].x, Frames[i].y, Frames[i].z );
			gluSphere( sphere, 0.05, 10, 10);
			glPopMatrix();
		}		
	}
	
	float frame;
	float vec[3];
	// Draw a line through the points
	glBegin( GL_LINE_STRIP ); {		
		for( i = 0; i < numLoops; i++ ) {
			frame = i*(1000.0/numLoops);
			
			findCurveLoc( frame, vec );
			
			glVertex3f( vec[0], vec[1], vec[2] );
		}
	} glEnd();
	
	glEndList();
	displayListCurrent = true;
}

void Hologram::reset() {
	tRange[0] = 0.0;
	tRange[1] = 1.0;
	
	transXYZ[0] =
		transXYZ[0] =
		transXYZ[0] = 0.0;
	
	numLoops = 250;
	// Re-seed the random number generator
	srandom( time(NULL) );
	
	displayListCurrent = false;
	textureCurrent = false;
	shaderCurrent = false;
	
	currentTime = glutGet( GLUT_ELAPSED_TIME );
}

void Hologram::animate() {
	int	lookFrame;
//	int bankFrame;
	
	int newTime = glutGet( GLUT_ELAPSED_TIME );
	int timePassed = newTime - currentTime;
	currentTime = newTime;
	
	currentFrame += (float)timePassed / 10.0;
	currentFrame %= 1000;
	
	lookFrame = currentFrame + 5;
	lookFrame %= 1000;
	
	findCurveLoc( currentFrame, eye );
	findCurveLoc( lookFrame, look );
	
}

void Hologram::checkboxCallback( int id ) {
	displayListCurrent = false;
//		textureCurrent = false;
}

void Hologram::sliderCallback( int id ) {	
	if( id == LOOPS || id == SPHERES ) {
		displayListCurrent = false;
	}
	
	glutPostRedisplay();
//	textureCurrent = false;
}

void Hologram::buttonCallback( int id ) {
	cout << "Scalar value: " << tRange[0] << " to " << tRange[1] << endl;	
}
