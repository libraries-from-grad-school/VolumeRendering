/*
 *  Keyframe.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 3/16/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

// Project includes
#include "Keyframe.h"
#include "Utility.h"
// Framework includes
#include <glui.h>
#include <GLUT/glut.h>
// Stdlib includes
#include <cmath>
#include <iostream>
#include <stdio.h>
using namespace std;

// Ugliness to deal with C++ and callbacks
static Keyframe *thisObject;

// the keyframes
struct keyframe
{
	int frame;					// frame #
	float x, y, z;				// x, y, and z locations of the eye
	float mS;					// minimum Scalar value
	float xS;					// maximum Scalar value
	float dxdf, dydf, dzdf;		// derivatives
	float dsmf;					// derivatives
	float dsxf;					// derivatives
	struct interval {			// Coons curve properties for the near-by intervals
		float dxdt, dydt, dzdt;		// derivatives
		float dsmt;					// derivatives
		float dsxt;					// derivatives
		float cX, cY, cZ;
		float dX, dY, dZ;
		float cmS, cxS, dmS, dxS;		
	} intervals[2];
} Frames[] = {
	{   0, -2.0284,  0.1692,  0.7967, 7.07805e-08, 1. },
	{  12, -1.9118,  0.1860,  0.1852, 7.07805e-08, 1. },
	{  75, -1.1811,  0.1453,  0.0053, 7.07805e-08, 1. },
	{ 187, -0.7336,  0.4087,  0.0053, 7.07805e-08, 1. },
	{ 350, -0.4906,  0.8833,  0.2331, 7.07805e-08, 1. },
	{ 422, -0.0557,  1.0454,  0.5509, 7.07805e-08, 1. },
	{ 455,  0.3220,  1.1482,  0.6828, 7.07805e-08, 1. },
	{ 487,  0.8415,  1.1760,  0.1013, 7.07805e-08, 1. },
	{ 500,  0.6488,  1.1714, -0.6722, 7.07805e-08, 1. },
	{ 562, -0.2438,  1.2561, -0.6841, 7.07805e-08, 1. },
	{ 625, -0.3390,  0.6897,  0.0173, 7.07805e-08, 1. },
	{ 687, -0.3335,  0.4227,  0.5509, 7.07805e-08, 1. },
	{ 750, -0.3430,  0.0966,  1.2344, 7.07805e-08, 1. },
	{ 812, -0.6192,  0.0000,  1.7979, 7.07805e-08, 1. },
	{ 875, -1.1703,  0.0000,  1.7380, 7.07805e-08, 1. },
	{ 937, -1.6030,  0.0687,  1.3783, 7.07805e-08, 1. },
	{  -1, -2.0284,  0.1692,  0.7967, 7.07805e-08, 1. } };

void
coonsCurveInterval( keyframe *last, keyframe *current, keyframe *next ) {
	current->intervals[0].dxdt = current->dxdf * (current->frame - last->frame);
	current->intervals[0].dydt = current->dydf * (current->frame - last->frame);
	current->intervals[0].dzdt = current->dzdf * (current->frame - last->frame);
	current->intervals[0].dsmt = current->dsmf * (current->frame - last->frame);
	current->intervals[0].dsxt = current->dsxf * (current->frame - last->frame);
	current->intervals[1].dxdt = current->dxdf * (next->frame - current->frame);
	current->intervals[1].dydt = current->dydf * (next->frame - current->frame);
	current->intervals[1].dzdt = current->dzdf * (next->frame - current->frame);
	current->intervals[1].dsmt = current->dsmf * (next->frame - current->frame);
	current->intervals[1].dsxt = current->dsxf * (next->frame - current->frame);
}
					
void
findDeriv( keyframe *last, keyframe *current, keyframe *next ) {
	float dnom = ( next->frame - last->frame );
// Find the keyframe derivitives
	current->dxdf  = (next->x  - last->x ) / dnom;
	current->dydf  = (next->y  - last->y ) / dnom;
	current->dzdf  = (next->z  - last->z ) / dnom;
	current->dsmf  = (next->mS - last->mS) / dnom;
	current->dsxf  = (next->xS - last->xS) / dnom;
// Find the per-frame derivitives for each adj. interval
	coonsCurveInterval( last, current, next );
}

// Constructor and destructor
#pragma mark [Con|De]structor
Keyframe::Keyframe( int newWidth, int newHeight, int newDepth ){
	int i;
	
	// Make a local copy of the dimensions of the data
	width = newWidth;
	height = newHeight;
	depth = newDepth;
	
	// Save a static copy of the self-referential pointer
	thisObject = this;
	
	// Allocate data for pre-computed dataset and compute the data points
	data = new float[ width * height * depth ];

	i = 0;
	do {
		Frames[i].x += 0.3;	 // These values correct the offset
		Frames[i].y += 0.2;	
	} while( Frames[++i].frame != -1 );

	i = 0;
	// Find the derivitives for all keyframes other than 0
	do {
		findDeriv( &(Frames[i-1]), &(Frames[i]), &(Frames[i+1]) );
	} while( Frames[++i].frame != -1 );
	i -= 1;
	// And again for keyframe 0 (now that the last keyframe is known)
	findDeriv( &(Frames[i]), &(Frames[0]), &(Frames[1]) );
	// Then for the last keyframe
	findDeriv( &(Frames[i-1]), &(Frames[i]), &(Frames[0]) );
	// And again for the sentry (isn't this fun!?)
	findDeriv( &(Frames[i]), &(Frames[i+1]), &(Frames[0]) );
	
	theUI = NULL;
	
	// Compute pre-cached data for each point
	this->computeData();
}

Keyframe::~Keyframe() {
	// Deallocate the storage for the dataset
	delete this->data;
}

//void
//Keyframe::setUI( VolumeUI *ui ) {
//	theUI = ui;
//}
// Dataset Creation and managment
#pragma mark -
#pragma mark Dataset managment
// Bounds of the dataset in model space
#define BOUNDS_LOW	 -1.0
#define BOUNDS_HIGH   1.0
#define SCALAR_HIGH 100.0
#define SCALAR_LOW	  0.0

struct centers {
	float xc, yc, zc;       // center location
	float a;                // amplitude
} Centers[] = {
	{ 1.00f, 0.00f, 0.00f,  90.00f },
	{-1.00f, 0.30f, 0.00f, 120.00f },
	{ 0.00f, 1.00f,	0.00f, 120.00f },
	{ 0.00f, 0.40f,	1.00f, 170.00f } };

inline float SQR( float in ) { return in * in; }

float
SValue( float x, float y, float z )
{
	int i;                  // counter
	float s;                // scalar value
	float dx, dy, dz, dsqd; // displacements
	
	s = 0.0;
	
	for( i=0; i < 4; i++ )
	{
		dx = x - Centers[i].xc;
		dy = y - Centers[i].yc;
		dz = z - Centers[i].zc;
		dsqd = SQR(dx) + SQR(dy) + SQR(dz);
		s += Centers[i].a * exp( -5.*dsqd );
	}
	
	return s / 160;
}

void Keyframe::computeData() {
	int i, j, k;
	
	for( i = 0; i < depth; i++ )
		for( j = 0; j < height; j++ )
			for( k = 0; k < width; k++ )
				data[k + (j * width) + (i * width * height)] =
					SValue( modelSpaceCoord( k, width,  BOUNDS_LOW, BOUNDS_HIGH ),
							modelSpaceCoord( j, height, BOUNDS_LOW, BOUNDS_HIGH ),
							modelSpaceCoord( i, depth,  BOUNDS_LOW, BOUNDS_HIGH ) );
	
}

// UI and Drawing routines
#pragma mark -
#pragma mark UI and Drawing routines

// ID's for the range sliders and their callback
#define SPHERES 1
#define TRANGE 4
#define CAPTURE 13
#define LOCATION 14
#define LOOPS 15

// Callback that can be used by GLUI
void Sliders( int id ) { thisObject->sliderCallback( id ); }
void Checkboxes( int id) { thisObject->checkboxCallback( id ); }
void Buttons(int id) { thisObject->buttonCallback( id ); }

// Create the GLUI widgets necessary for this model
void Keyframe::initUI( GLUI *glui ) {
	GLUI_RangeSlider *range;
//	GLUI_Scrollbar *scroll;
	
	// T filter values
	range = new GLUI_RangeSlider( glui, "Temp Values", tRange, TRANGE, Sliders );
	range->set_float_limits( 0.0, 1.0 );
	new GLUI_StaticText( glui, "T Points Range" );
	
//	scroll = new GLUI_Scrollbar( glui, "Number of loops", GLUI_SCROLL_HORIZONTAL, &numLoops, LOOPS, Sliders );
//	scroll->set_int_limits( 5, 500 );
	new GLUI_StaticText( glui, "Number of line segments" );
	
	new GLUI_Checkbox( glui, "Draw Spheres", (int*)&spheres, SPHERES, Sliders );
	
	// Create widgets to manipulate the modelview matrix
	GLUI_Panel *modelviewPanel = new GLUI_Panel( glui, "Modelview transformations" ); {
		// XY Translator
		new GLUI_Column( modelviewPanel, false );
		GLUI_Translation *transXY = new GLUI_Translation( modelviewPanel, "XY Translation", GLUI_TRANSLATION_XY, &transXYZ[0], LOCATION, Sliders );
		transXY->set_speed( 0.01 );
		
		// Z Translator
		new GLUI_Column( modelviewPanel, false );
		GLUI_Translation *transZ = new GLUI_Translation( modelviewPanel, "Z Translation", GLUI_TRANSLATION_Z, &transXYZ[2], LOCATION, Sliders );
		transZ->set_speed( 0.01 );
	}
	
	// Button to capture state for a keyframe
	new GLUI_Button( glui, "Capture State for Keyframe", CAPTURE, Buttons );
	
	// Get a quadric for use as the keyframe sphere
	sphere = gluNewQuadric();
	gluQuadricDrawStyle( sphere, GLU_FILL );
	// Get OpenGL shader program ID's
	vertexShaderID = glCreateShader( GL_VERTEX_SHADER );
	fragShaderID = glCreateShader( GL_FRAGMENT_SHADER );
	xferFuncProgram = glCreateProgram();
	// Get OpenGL retained mode graphics ID's
	volumeDisplayList = glGenLists( 1 );// Request display list ID from OpenGL
	glEnable (GL_TEXTURE_RECTANGLE_ARB);// Enable texture rectangles
	glGenTextures(2, texID);			// Request texture ID from OpenGL
}

// Callbacks
void Keyframe::draw() {	
	GLuint scalarVal;
	if( !shaderCurrent )
		this->loadShaders();

	if( !textureCurrent )
		this->refreshTexture();
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	gluLookAt( eye[0], eye[1], eye[2], look[0], look[1], look[2], 0.0, 1.0, 0.0 );
	
	glUseProgram( xferFuncProgram );
	// Set the filtering uniforms
	scalarVal = glGetUniformLocation( xferFuncProgram, "minScalar" );
	glUniform1f( scalarVal, tRange[0] );
	scalarVal = glGetUniformLocation( xferFuncProgram, "maxScalar" );
	glUniform1f( scalarVal, tRange[1] );	
	
	glDisable( GL_VERTEX_PROGRAM_POINT_SIZE );

	if( displayListCurrent )
		glCallList( volumeDisplayList );
	else
		this->internalDraw();
	
	glPopMatrix();
	
	return;
}

#define BLOCK_SIZE 4096
void Keyframe::loadShaders( void ) {
//	const char vertexFileName = "xferFunc.vert"
//	const char fragFileName = "xferFunc.frag"
	GLint status;
	unsigned int readBytes = 0;
	char shaderFileBuffer[4096];
	const GLchar *shaderBuffer[1] = { (GLchar*)shaderFileBuffer };
	
	FILE *fileID;
	
	fileID = fopen( "xferFunc.vert", "r" ); // Open the file for reading
	if( fileID == NULL ) {
		printf("Error opening file: %s\n");//, vertexFileName);
	} else {
		readBytes = fread( (void *)shaderFileBuffer, 1, BLOCK_SIZE, fileID );
		if( readBytes == BLOCK_SIZE ) {
			if( shaderFileBuffer[BLOCK_SIZE-1] != EOF )
				cout << "Vertex shader too large" << endl;
		} else {
			glShaderSource( vertexShaderID, 1, shaderBuffer, (GLint*)&readBytes );		
		}
		
		fclose( fileID );		
	}
	
	glCompileShader( vertexShaderID );
	glGetShaderiv( vertexShaderID, GL_COMPILE_STATUS, &status );
	if( status != GL_TRUE ) {
		glGetShaderInfoLog( vertexShaderID, BLOCK_SIZE, (GLsizei*)&readBytes, shaderFileBuffer );
		cout << shaderFileBuffer << endl;
	}
	glAttachShader( xferFuncProgram, vertexShaderID );
	
	fileID = fopen( "xferFunc.frag", "r" ); // Open the file for reading
	if( fileID == NULL ) {
		printf("Error opening file: %s\n");//, fragFileName);
	} else {
		readBytes = fread( (void *)shaderFileBuffer, 1, BLOCK_SIZE, fileID );
		if( readBytes == BLOCK_SIZE ) {
			if( shaderFileBuffer[BLOCK_SIZE-1] != EOF )
				cout << "Fragment shader too large" << endl;
		} else {
			glShaderSource( fragShaderID, 1, shaderBuffer, (GLint*)&readBytes );		
		}
		
		fclose( fileID );	
	}	
	
	glCompileShader( fragShaderID );
	glGetShaderiv( fragShaderID, GL_COMPILE_STATUS, &status );
	if( status != GL_TRUE ) {
		glGetShaderInfoLog( fragShaderID, BLOCK_SIZE, (GLsizei*)&readBytes, shaderFileBuffer );
		cout << shaderFileBuffer << endl;
	}
	glAttachShader( xferFuncProgram,   fragShaderID );	

	glLinkProgram ( xferFuncProgram );
	glValidateProgram( xferFuncProgram );
	glGetProgramiv( xferFuncProgram, GL_VALIDATE_STATUS, &status );
	if( status != GL_TRUE ) {
		glGetProgramInfoLog( xferFuncProgram, BLOCK_SIZE, (GLsizei*)&readBytes, shaderFileBuffer );
		cout << "Error while validating program:" << endl << shaderFileBuffer << endl;
	}	
	
	shaderCurrent = true;
}

void Keyframe::refreshTexture( void ) {
	GLfloat xferFunc[7][4] = {
		{ 0.0, 0.0, 0.0, 0.0 },
		{ 0.0, 0.0, 1.0, 1.0 },
		{ 0.0, 1.0, 1.0, 1.0 },
		{ 0.0, 1.0, 0.0, 1.0 },
		{ 1.0, 1.0, 0.0, 1.0 },
		{ 1.0, 0.0, 0.0, 1.0 },
		{ 0.0, 0.0, 0.0, 0.0 }
	};
	
	// Apple specific extensions to improve performance (breaks shaders?)
//	glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_STORAGE_HINT_APPLE, GL_STORAGE_SHARED_APPLE);	

	// Choose the texture id to set
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_3D, texID[0] );
	// Load texture data
	glTexImage3D(GL_TEXTURE_3D, 0, GL_LUMINANCE, width, height, depth, 0, GL_LUMINANCE, GL_FLOAT, data );
	// Texture Modes
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
	
	glActiveTexture( GL_TEXTURE1 );
	glBindTexture( GL_TEXTURE_1D, texID[1] );
	// Load transfer func data
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, 7, 1, GL_RGBA, GL_FLOAT, xferFunc );
	// Texture modes
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		
	textureCurrent = true;
}

void findCurveLoc( int frameNum, float vec[3] ) {
	float cX, dX, cY, dY, cZ, dZ;
	float t, framesThisInterval;
	keyframe  *currKeyFrame = &Frames[0];
	
		// Find the keyframes that "most recently passed" for the eye
	while( (currKeyFrame+1)->frame < frameNum && (currKeyFrame+1)->frame != -1 )
		currKeyFrame++;
	
	if( (currKeyFrame+1)->frame == (-1) ) {
		framesThisInterval = (float)(Frames[0].frame - currKeyFrame->frame);
	} else {
		framesThisInterval = (float)((currKeyFrame+1)->frame - currKeyFrame->frame);
	}
	
	t = (float)( frameNum - currKeyFrame->frame ) / framesThisInterval;
	
	cX = -3.0 * currKeyFrame->x + 3.0 * (currKeyFrame+1)->x - 2.0 * currKeyFrame->intervals[1].dxdt - (currKeyFrame+1)->intervals[0].dxdt;
	cY = -3.0 * currKeyFrame->y + 3.0 * (currKeyFrame+1)->y - 2.0 * currKeyFrame->intervals[1].dydt - (currKeyFrame+1)->intervals[0].dydt;
	cZ = -3.0 * currKeyFrame->z + 3.0 * (currKeyFrame+1)->z - 2.0 * currKeyFrame->intervals[1].dzdt - (currKeyFrame+1)->intervals[0].dzdt;
	dX =  2.0 * currKeyFrame->x - 2.0 * (currKeyFrame+1)->x + currKeyFrame->intervals[1].dxdt + (currKeyFrame+1)->intervals[0].dxdt;
	dY =  2.0 * currKeyFrame->y - 2.0 * (currKeyFrame+1)->y + currKeyFrame->intervals[1].dydt + (currKeyFrame+1)->intervals[0].dydt;
	dZ =  2.0 * currKeyFrame->z - 2.0 * (currKeyFrame+1)->z + currKeyFrame->intervals[1].dzdt + (currKeyFrame+1)->intervals[0].dzdt;
	
	vec[0] = currKeyFrame->x + t * (currKeyFrame->intervals[1].dxdt + t * ( cX + t * dX ) );
	vec[1] = currKeyFrame->y + t * (currKeyFrame->intervals[1].dydt + t * ( cY + t * dY ) );
	vec[2] = currKeyFrame->z + t * (currKeyFrame->intervals[1].dzdt + t * ( cZ + t * dZ ) );
	
}

void Keyframe::internalDraw() {
	GLuint texLoc;
	float u, v, w;
	int i, j, k;
	
	glNewList( volumeDisplayList, GL_COMPILE_AND_EXECUTE );

	// Assume that the shader program is loaded and 'used'
	texLoc = glGetUniformLocation( xferFuncProgram, "scalars" );
	glUniform1i( texLoc, 0 );
	texLoc = glGetUniformLocation( xferFuncProgram, "xferFunc" );
	glUniform1i( texLoc, 1 );
	texLoc = glGetUniformLocation( xferFuncProgram, "filter" );
	glUniform1i( texLoc, GL_TRUE );

	glPointSize( 3.0 );
	
	// Point cloud
	glBegin( GL_POINTS );
	for( i = 0; i < depth / 2; i++ ) {
		for( j = 0; j < height / 2; j++ ) {
			for( k = 0; k < width / 2; k++ ) {
				// Re-calculate the u,v,w values with a random offset
				u = modelSpaceCoord( float(k) + randomOffset(), width / 2,  BOUNDS_LOW, BOUNDS_HIGH );
				v = modelSpaceCoord( float(j) + randomOffset(), height / 2, BOUNDS_LOW, BOUNDS_HIGH );
				w = modelSpaceCoord( float(i) + randomOffset(), depth / 2,  BOUNDS_LOW, BOUNDS_HIGH );
				
				// Place the point
				//glTexCoord3f( (u + 1) / 2, (v + 1) / 2, (w + 1) / 2 );	
				glVertex3f( u, v, w );
			}
		}
	}
	glEnd();

	texLoc = glGetUniformLocation( xferFuncProgram, "filter" );
	glUniform1i( texLoc, GL_FALSE );
	// Turn-off the shader
//	glUseProgram( 0 );	
	// Keyframe spheres
	if( spheres ) {
		for( i = 0; Frames[i].frame != -1; i++ ) {
			glPushMatrix();
			glTranslatef( Frames[i].x, Frames[i].y, Frames[i].z );
			gluSphere( sphere, 0.05, 10, 10);
			glPopMatrix();
		}		
	}

	float frame;
	float vec[3];
	// Draw a line through the points
	glBegin( GL_LINE_STRIP ); {		
		for( i = 0; i < numLoops; i++ ) {
			frame = i*(1000.0/numLoops);
			
			findCurveLoc( frame, vec );
			
			glVertex3f( vec[0], vec[1], vec[2] );
		}
	} glEnd();
	
	glEndList();
	displayListCurrent = true;
}

void Keyframe::reset() {
	tRange[0] = 0.0;
	tRange[1] = 1.0;
		
	transXYZ[0] =
	transXYZ[0] =
	transXYZ[0] = 0.0;
	
	numLoops = 250;
	// Re-seed the random number generator
	srandom( time(NULL) );
	
	displayListCurrent = false;
	textureCurrent = false;
	shaderCurrent = false;

	currentTime = glutGet( GLUT_ELAPSED_TIME );
}

void Keyframe::animate() {
	int	lookFrame;
//	int bankFrame;
	
	int newTime = glutGet( GLUT_ELAPSED_TIME );
	int timePassed = newTime - currentTime;
	currentTime = newTime;
	
	currentFrame += (float)timePassed / 10.0;
	currentFrame %= 1000;

	lookFrame = currentFrame + 5;
	lookFrame %= 1000;
	
	findCurveLoc( currentFrame, eye );
	findCurveLoc( lookFrame, look );
	
}

void Keyframe::checkboxCallback( int id ) {
	displayListCurrent = false;
//		textureCurrent = false;
}

void Keyframe::sliderCallback( int id ) {	
	if( id == LOOPS || id == SPHERES ) {
		displayListCurrent = false;
	}
	
	glutPostRedisplay();
//	textureCurrent = false;
}

void Keyframe::buttonCallback( int id ) {
	cout << "Scalar value: " << tRange[0] << " to " << tRange[1] << endl;	
}
