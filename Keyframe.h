/*
 *  Keyframe.h
 *  VolumeRender
 *
 *  Created by William Dillon on 3/16/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include "Utility.h"
#include "Model.h"
#include "VolumeUI.h"

#ifndef KEYFRAME_H
#define KEYFRAME_H

class Keyframe : public Model {
public:
	// Constructor
	Keyframe( int newWidth, int newHeight, int newDepth );
	~Keyframe();
	
	// UI Creation
	void initUI( GLUI *glui );
//	void setUI( VolumeUI *currentUI );
	
	// Callbacks
	void sliderCallback( int id );
	void checkboxCallback( int id );
	void buttonCallback( int id );
	void draw();
	void reset();
	void animate();
	
	int width, height, depth;
	float getData( int u, int v, int w );
	// Note that this form of getData does not used cached values
	float getData( float u, float v, float w );

	// UI class refrences
//	VolumeUI *theUI;
	GLUI *glui;

private:
	// Functions to create and manage the dataset
	void computeData();
	
	// Animation parameters
	int	currentFrame;	// 1ms of time per frame
	int currentTime;	// Used to calculate time passage
	float eye[3], look[3];
	// Parameters for point cloud
//	int points, jitter;
	float tRange[2];
	
	// State variables pertaining to and the data
	GLuint texID[2];
	float *data;
	bool textureCurrent;
	void refreshTexture( void );

	// Stuff to deal with shaders
	bool shaderCurrent;
	void loadShaders( void );
	GLuint xferFuncProgram;
	GLuint fragShaderID;
	GLuint vertexShaderID;
	
	// Use display lists whenever possible
	int numLoops;
	GLUquadric *sphere;
	bool spheres;
	int volumeDisplayList;
	bool displayListCurrent;
	void internalDraw( void );
	float transXYZ[3];
};
#endif