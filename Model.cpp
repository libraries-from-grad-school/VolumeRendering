/*
 *  Model.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 2/2/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "Model.h"

void Model::initUI( GLUI *glui ) {
	return;
}

void Model::draw( ) {
	return;
}

void Model::animate() {
	return;
}

void Model::reset() {
	return;
}

void Model::keyboard( char k, int m ) {
	printf( "ignoring key '%c'\n", k );
	
	return;
}

void Model::setUI( VolumeUI *currentUI ) {
	theUI = currentUI;
}

VolumeUI *
Model::getUI( void ) {
	return theUI;
}