/*
 *  Model.h
 *  VolumeRender
 *
 *  Created by William Dillon on 2/1/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "glui.h"

#ifndef MODEL_H
#define MODEL_H

class VolumeUI;

// Superclass for the volume model

class Model {
public:
	
	virtual void initUI( GLUI *glui );
	virtual void draw();
	virtual void animate();
	virtual void reset();
	virtual void keyboard(char k, int m);
	
	void setUI( VolumeUI *currentUI );
	VolumeUI * getUI( void );
	VolumeUI *theUI;
};
#endif