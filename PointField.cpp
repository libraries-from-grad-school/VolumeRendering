/*
 *  PointField.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 2/1/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "PointField.h"
#include "Utility.h"
#include <cmath>
#include <glui.h>
#include <GLUT/glut.h>

// Ugliness to deal with C++ and callbacks
static PointField *thisObject;

// Constructor and destructor
#pragma mark [Con|De]structor
PointField::PointField( int newWidth, int newHeight, int newDepth ){
	// Make a local copy of the dimensions of the data
	width = newWidth;
	height = newHeight;
	depth = newDepth;

	// Save a static copy of the self-referential pointer
	thisObject = this;
	
	// Allocate data for pre-computed dataset and compute the data points
#ifdef UINT
	data = new unsigned char[ width * height * depth * 4 ];
#else
	data = new float[ width * height * depth * 4 ];
#endif
	
	// Compute pre-cached data for each point
	this->computeData();
}

PointField::~PointField() {
	// Deallocate the storage for the dataset
	delete this->data;
}

// Dataset Creation and managment
#pragma mark -
#pragma mark Dataset managment
// Bounds of the dataset in model space
#define BOUNDS_LOW	 -1.0
#define BOUNDS_HIGH   1.0
#define SCALAR_HIGH 100.0
#define SCALAR_LOW	  0.0

struct centers {
	float xc, yc, zc;       // center location
	float a;                // amplitude
} Centers[] = {
	{ 1.00f, 0.00f, 0.00f,  90.00f },
	{-1.00f, 0.30f, 0.00f, 120.00f },
	{ 0.00f, 1.00f,	0.00f, 120.00f },
	{ 0.00f, 0.40f,	1.00f, 170.00f } };

inline float SQR( float in ) { return in * in; }

float
SValue( float x, float y, float z )
{
	int i;                  // counter
	float s;                // scalar value
	float dx, dy, dz, dsqd; // displacements
	
	s = 0.0;
	
	for( i=0; i < 4; i++ )
	{
		dx = x - Centers[i].xc;
		dy = y - Centers[i].yc;
		dz = z - Centers[i].zc;
		dsqd = SQR(dx) + SQR(dy) + SQR(dz);
		s += Centers[i].a * exp( -5.*dsqd );
	}
	
	return s;
}

void PointField::setData( int x, int y, int z, float scalar ) {
	float RGB[3];
	scalar2RGB( scalar, SCALAR_HIGH, SCALAR_LOW, RGB );

#ifdef UINT
	data[1 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = (unsigned char)( RGB[2] * 255.0 );
	data[2 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = (unsigned char)( RGB[1] * 255.0 );
	data[3 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = (unsigned char)( RGB[0] * 255.0 );
	data[0 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = (unsigned char)( 255 );
#else	
	data[0 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = RGB[0];
	data[1 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = RGB[1];
	data[2 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = RGB[2];
	data[3 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = 1.0;
#endif
}

void PointField::computeData() {
	int i, j, k;

	for( i = 0; i < depth; i++ )
		for( j = 0; j < height; j++ )
			for( k = 0; k < width; k++ )
				setData( k, j, i, SValue( modelSpaceCoord( k, width,  BOUNDS_LOW, BOUNDS_HIGH ),
										  modelSpaceCoord( j, height, BOUNDS_LOW, BOUNDS_HIGH ),
										  modelSpaceCoord( i, depth,  BOUNDS_LOW, BOUNDS_HIGH ) ) );
	
}

// UI and Drawing routines
#pragma mark -
#pragma mark UI and Drawing routines

// ID's for the range sliders and their callback
#define POINTS_CHECKS 1
#define SLICES_CHECKS 2
#define CONTOURS_CHECKS 3
#define XRANGE 1
#define YRANGE 2
#define ZRANGE 3
#define TRANGE 4
#define XSLICE 5
#define YSLICE 6
#define ZSLICE 7
#define XCONTOUR 8
#define YCONTOUR 9
#define ZCONTOUR 10
#define CONTOUR 11
#define ISOSURFACE 12

// Callback that can be used by GLUI
void Sliders( int id ) { thisObject->sliderCallback( id ); }
void Checkboxes( int id) { thisObject->checkboxCallback( id ); }

// Create the GLUI widgets necessary for this model
void PointField::initUI( GLUI *glui ) {
	GLUI_RangeSlider *range;
	GLUI_Scrollbar *slice;

	// Parameters for point cloud
	GLUI_Rollout *pointsPanel = new GLUI_Rollout( glui, "Points", false ); {
		// Display the data as points?
		new GLUI_Checkbox( pointsPanel, "Points", &points, POINTS_CHECKS, Checkboxes );
		// Options for points display
		new GLUI_Checkbox( pointsPanel, "Jitter", &jitter, POINTS_CHECKS, Checkboxes );
		
		// X filter values
		range = new GLUI_RangeSlider( pointsPanel, "X Values", xRange, XRANGE, Sliders );
		range->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
		new GLUI_StaticText( pointsPanel, "X Points Range" );
		// Y filter values
		range = new GLUI_RangeSlider( pointsPanel, "Y Values", yRange, YRANGE, Sliders );
		range->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
		new GLUI_StaticText( pointsPanel, "Y Points Range" );
		// Z filter values
		range = new GLUI_RangeSlider( pointsPanel, "Z Values", zRange, ZRANGE, Sliders );
		range->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
		new GLUI_StaticText( pointsPanel, "Z Points Range" );
		// T filter values
		range = new GLUI_RangeSlider( pointsPanel, "Temp Values", tRange, TRANGE, Sliders );
		range->set_float_limits( SCALAR_LOW, SCALAR_HIGH );		
		new GLUI_StaticText( pointsPanel, "T Points Range" );
	}

	// Parameters for sliceplanes
	GLUI_Rollout *slicePanel = new GLUI_Rollout( glui, "Slices" ); {
		// Display a YZ plane?
		new GLUI_Checkbox( slicePanel, "YZ Slice", &sliceYZ, SLICES_CHECKS, Checkboxes );
		slice = new GLUI_Scrollbar( slicePanel, "X value of slice", GLUI_SCROLL_HORIZONTAL, &xSlice, XSLICE, Sliders );
		slice->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
		new GLUI_StaticText( slicePanel, "X Slice value" );
		// Display a XZ plane?
		new GLUI_Checkbox( slicePanel, "XZ Slice", &sliceXZ, SLICES_CHECKS, Checkboxes );
		slice = new GLUI_Scrollbar( slicePanel, "Y value of slice", GLUI_SCROLL_HORIZONTAL, &ySlice, YSLICE, Sliders );
		slice->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
		new GLUI_StaticText( slicePanel, "Y Slice value" );
		// Display a XY plane?
		new GLUI_Checkbox( slicePanel, "XY Slice", &sliceXY, SLICES_CHECKS, Checkboxes );
		slice = new GLUI_Scrollbar( slicePanel, "Z value of slice", GLUI_SCROLL_HORIZONTAL, &zSlice, ZSLICE, Sliders );
		slice->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
		new GLUI_StaticText( slicePanel, "Z Slice value" );		
	}

	// Parameters for contours
	GLUI_Rollout *contourPanel = new GLUI_Rollout( glui, "Contours" ); {
		slice = new GLUI_Scrollbar( contourPanel, "Number of contours", GLUI_SCROLL_HORIZONTAL, &numContours, CONTOUR, Sliders );
		slice->set_int_limits( 0, 25 );		
		// Display a YZ contours?
		new GLUI_Checkbox( contourPanel, "YZ Contours", &contourYZ, CONTOURS_CHECKS, Checkboxes );
		slice = new GLUI_Scrollbar( contourPanel, "X value of contours", GLUI_SCROLL_HORIZONTAL, &xContour, XCONTOUR, Sliders );
		slice->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
//		new GLUI_StaticText( slicePanel, "X Contour value" );
		// Display a XZ contours?
		new GLUI_Checkbox( contourPanel, "XZ Contours", &contourXZ, CONTOURS_CHECKS, Checkboxes );
		slice = new GLUI_Scrollbar( contourPanel, "Y value of contours", GLUI_SCROLL_HORIZONTAL, &yContour, YCONTOUR, Sliders );
		slice->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
//		new GLUI_StaticText( slicePanel, "Y Contour value" );
		// Display a XY contours?
		new GLUI_Checkbox( contourPanel, "XY Contours", &contourXY, CONTOURS_CHECKS, Checkboxes );
		slice = new GLUI_Scrollbar( contourPanel, "Z value of contours", GLUI_SCROLL_HORIZONTAL, &zContour, ZCONTOUR, Sliders );
		slice->set_float_limits( BOUNDS_LOW, BOUNDS_HIGH );
//		new GLUI_StaticText( slicePanel, "Z Contour value" );				
	}

	GLUI_Rollout *isoSurfacePanel = new GLUI_Rollout( glui, "Isosurface" ); {
		new GLUI_Checkbox( isoSurfacePanel, "Isosurface wireframe", &isoSurface, ISOSURFACE, Checkboxes );
//		slice = new GLUI_Scrollbar( isoSurfacePanel, "Isosurface Slab Size", GLUI_SCROLL_HORIZONTAL, &numSlices, ISOSURFACE, Sliders );
//		slice->set_int_limits( 0, 25 );	
		slice = new GLUI_Scrollbar( isoSurfacePanel, "ISO value of surface", GLUI_SCROLL_HORIZONTAL, &isoValue, ISOSURFACE, Sliders );
		slice->set_float_limits( SCALAR_LOW, SCALAR_HIGH );			
	}
	
	// Get OpenGL retained mode graphics ID's
	volumeDisplayList = glGenLists( 1 );// Request display list ID from OpenGL
	glEnable (GL_TEXTURE_RECTANGLE_ARB);// Enable texture rectangles
	glGenTextures(1, &texID);			// Request texture ID from OpenGL

}

// Callbacks
void PointField::draw() {
	if( textureCurrent && displayListCurrent ) {
		glCallList( volumeDisplayList );
		return;
	}
	
	if( !textureCurrent )
		this->refreshTexture();

	if( displayListCurrent )
		glCallList( volumeDisplayList );
	else
		this->internalDraw();

	return;
}

#define RANGE
#define CLIENT_STORE

void PointField::refreshTexture( void ) {
	int runSizes[] = { 10, 15, 16, 20, 30, 32, 40, 60, 64, 80, 120, 128 };
	int numRuns = 12;
	int timeMS, tempTime, i, w, h, d;
	
	glBindTexture( GL_TEXTURE_3D, texID );

#ifdef CLIENT_STORE
	glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
#endif
	
	glFinish();
	timeMS = glutGet( GLUT_ELAPSED_TIME );
	for( i = 0; i < numRuns; i++ ) {
		w = h = d = runSizes[i];
#ifdef UINT
#ifdef RANGE
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_STORAGE_HINT_APPLE, GL_STORAGE_SHARED_APPLE);
		glTextureRangeAPPLE(GL_TEXTURE_3D, w * h * d * 4 * sizeof(unsigned char), data);
#endif
		glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, w, h, d, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, data );
		
#else
#ifdef RANGE
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_STORAGE_HINT_APPLE, GL_STORAGE_SHARED_APPLE);
		glTextureRangeAPPLE(GL_TEXTURE_3D, w * h * d * 4 * sizeof(float), data);
#endif
		glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, w, h, d, 0, GL_RGBA, GL_FLOAT, data );
#endif
		glFinish();
		tempTime = glutGet( GLUT_ELAPSED_TIME );
		fprintf( stderr, "Time elapsed for %d: %d.\n", runSizes[i], tempTime - timeMS );
	}

//	textureCurrent = true;
}

void PointField::internalDraw() {
	float u, v, w, s, deltaS, deltaM, RGB[3];
	int i, j, k;
	
	glNewList( volumeDisplayList, GL_COMPILE_AND_EXECUTE );

	// Set texture parameters
	glEnable( GL_TEXTURE_3D );
	glBindTexture( GL_TEXTURE_3D, texID );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
	
	glPointSize( 2.0 );

	// Point cloud
	if( points ) {
		glBegin( GL_POINTS );
		for( i = 0; i < depth / 2; i++ ) {
			w = modelSpaceCoord( i, depth / 2, BOUNDS_LOW, BOUNDS_HIGH );
			// Apply the Z range filter
			if( w < zRange[0] || w > zRange[1] )
				continue;
			
			for( j = 0; j < height / 2; j++ ) {
				v = modelSpaceCoord( j, height / 2, BOUNDS_LOW, BOUNDS_HIGH );
				// Apply the Y range filter
				if( v < yRange[0] || v > yRange[1] )
					continue;
				
				for( k = 0; k < width / 2; k++ ) {
					u = modelSpaceCoord( k, width / 2, BOUNDS_LOW, BOUNDS_HIGH );
					// Apply the X range filter
					if( u < xRange[0] || u > xRange[1] )
						continue;
					// Apply the temperature filter
					//if( getData( k, j, i ) < tRange[0] ||
					//	getData( k, j, i ) > tRange[1] )
					//	continue;
					// Later: apply the radius filter
					
					if( jitter ) {
						// Re-calculate the u,v,w values with a random offset
						u = modelSpaceCoord( float(k) + randomOffset(), width / 2,  BOUNDS_LOW, BOUNDS_HIGH );
						v = modelSpaceCoord( float(j) + randomOffset(), height / 2, BOUNDS_LOW, BOUNDS_HIGH );
						w = modelSpaceCoord( float(i) + randomOffset(), depth / 2,  BOUNDS_LOW, BOUNDS_HIGH );
					} 
					
					// Place the point
					glTexCoord3f( (u + 1) / 2, (v + 1) / 2, (w + 1) / 2 );	
					glVertex3f( u, v, w );
				}
			}
		}
		glEnd();
	}
	
	// Slicing planes
	if( sliceXY || sliceXZ || sliceYZ ) {
		glBegin( GL_QUADS ); {
			if( sliceXY ) {
				glTexCoord3f( 0.0, 0.0, (zSlice + 1) / 2 );
				glVertex3f  ( BOUNDS_LOW,  BOUNDS_LOW,  zSlice );
				glTexCoord3f( 0.0, 1.0, (zSlice + 1) / 2 );
				glVertex3f  ( BOUNDS_LOW,  BOUNDS_HIGH, zSlice );
				glTexCoord3f( 1.0, 1.0, (zSlice + 1) / 2 );
				glVertex3f  ( BOUNDS_HIGH, BOUNDS_HIGH, zSlice );
				glTexCoord3f( 1.0, 0.0,  (zSlice + 1) / 2 );
				glVertex3f  ( BOUNDS_HIGH, BOUNDS_LOW,  zSlice );
			}
			if( sliceXZ ) {
				glTexCoord3f( 0.0, (ySlice + 1) / 2, 0.0  );
				glVertex3f  ( BOUNDS_LOW,  ySlice, BOUNDS_LOW  );
				glTexCoord3f( 0.0, (ySlice + 1) / 2, 1.0 );
				glVertex3f  ( BOUNDS_LOW,  ySlice, BOUNDS_HIGH );
				glTexCoord3f( 1.0, (ySlice + 1) / 2, 1.0 );
				glVertex3f  ( BOUNDS_HIGH, ySlice, BOUNDS_HIGH );
				glTexCoord3f( 1.0, (ySlice + 1) / 2, 0.0  );
				glVertex3f  ( BOUNDS_HIGH, ySlice, BOUNDS_LOW  );
			}
			if( sliceYZ ) {
				glTexCoord3f( (xSlice + 1) / 2, 0.0, 0.0  );
				glVertex3f  ( xSlice, BOUNDS_LOW,  BOUNDS_LOW  );
				glTexCoord3f( (xSlice + 1) / 2, 0.0, 1.0 );
				glVertex3f  ( xSlice, BOUNDS_LOW,  BOUNDS_HIGH );
				glTexCoord3f( (xSlice + 1) / 2, 1.0, 1.0 );
				glVertex3f  ( xSlice, BOUNDS_HIGH, BOUNDS_HIGH );
				glTexCoord3f( (xSlice + 1) / 2, 1.0, 0.0  );
				glVertex3f  ( xSlice, BOUNDS_HIGH, BOUNDS_LOW  );
			}			
		} glEnd();
	}
	
	glDisable( GL_TEXTURE_3D );

	// Contour lines on a plane
	if( contourXY || contourXZ || contourYZ ) {
		glBegin( GL_LINES );
		glColor3f( 1.0, 1.0, 1.0 );

		deltaS = (SCALAR_HIGH - SCALAR_LOW) / numContours;
		// Draw the countour lines
		if( contourXY ) {
			for( i = 0, s = SCALAR_LOW; i < numContours; i++, s += deltaS)
				this->drawContour( ZCONTOUR, s, xContour, yContour, zContour );			
		}
		if( contourXZ ) {
			for( i = 0, s = SCALAR_LOW; i < numContours; i++, s += deltaS)
				this->drawContour( YCONTOUR, s, xContour, yContour, zContour );			
		}
		if( contourYZ ) {
			for( i = 0, s = SCALAR_LOW; i < numContours; i++, s += deltaS)
				this->drawContour( XCONTOUR, s, xContour, yContour, zContour );			
		}
		glEnd();
	}

	// Wireframe isosurfaces
	if( isoSurface ) {
		glBegin( GL_LINES );
		scalar2RGB( isoValue, SCALAR_HIGH, SCALAR_LOW, RGB );
		glColor3fv( RGB );

		deltaM = (BOUNDS_HIGH - BOUNDS_LOW) / numSlices;
		for( i = 0, s = BOUNDS_LOW; i < numSlices; i++, s += deltaM ) {
			this->drawContour( XCONTOUR, isoValue, s, yContour, zContour );
			this->drawContour( YCONTOUR, isoValue, xContour, s, zContour );
			this->drawContour( ZCONTOUR, isoValue, xContour, yContour, s );			
		}
		glEnd();
	}
	
	glEndList();
	displayListCurrent = true;
}


void PointField::drawContour( int currentPlane, float scalar, float xPlane, float yPlane, float zPlane ) {
	int i, j;
	
	if( currentPlane == ZCONTOUR ) {
		// Generate the contour slicing plane
		for( i = 0; i < NUMNODES; i++ ) {
			for( j = 0; j < NUMNODES; j++ ) {
				planeXY[i][j].x = modelSpaceCoord( i, NUMNODES, BOUNDS_LOW, BOUNDS_HIGH );
				planeXY[i][j].y = modelSpaceCoord( j, NUMNODES, BOUNDS_LOW, BOUNDS_HIGH );
				planeXY[i][j].z = zPlane;
				planeXY[i][j].s = SValue( planeXY[i][j].x, planeXY[i][j].y, planeXY[i][j].z );
			}
		}
		for( i = 0; i < NUMNODES - 1; i++ ) {
			for( j = 0; j < NUMNODES - 1; j++ ) {
				contourQuad( &(planeXY[i][j]), &(planeXY[i+1][j]), &(planeXY[i+1][j+1]), &(planeXY[i][j+1]), scalar );
			}
		}
	}	
	if( currentPlane == YCONTOUR ) {
		// Generate the contour slicing plane
		for( i = 0; i < NUMNODES; i++ ) {
			for( j = 0; j < NUMNODES; j++ ) {
				planeXY[i][j].x = modelSpaceCoord( i, NUMNODES, BOUNDS_LOW, BOUNDS_HIGH );
				planeXY[i][j].y = yPlane;
				planeXY[i][j].z = modelSpaceCoord( j, NUMNODES, BOUNDS_LOW, BOUNDS_HIGH );
				planeXY[i][j].s = SValue( planeXY[i][j].x, planeXY[i][j].y, planeXY[i][j].z );
			}
		}
		for( i = 0; i < NUMNODES - 1; i++ ) {
			for( j = 0; j < NUMNODES - 1; j++ ) {
				contourQuad( &(planeXY[i][j]), &(planeXY[i+1][j]), &(planeXY[i+1][j+1]), &(planeXY[i][j+1]), scalar );
			}
		}
	}	
	if( currentPlane == XCONTOUR ) {
		// Generate the contour slicing plane
		for( i = 0; i < NUMNODES; i++ ) {
			for( j = 0; j < NUMNODES; j++ ) {
				planeXY[i][j].x = xPlane;
				planeXY[i][j].y = modelSpaceCoord( i, NUMNODES, BOUNDS_LOW, BOUNDS_HIGH );
				planeXY[i][j].z = modelSpaceCoord( j, NUMNODES, BOUNDS_LOW, BOUNDS_HIGH );
				planeXY[i][j].s = SValue( planeXY[i][j].x, planeXY[i][j].y, planeXY[i][j].z );
			}
		}
		for( i = 0; i < NUMNODES - 1; i++ ) {
			for( j = 0; j < NUMNODES - 1; j++ ) {
				contourQuad( &(planeXY[i][j]), &(planeXY[i+1][j]), &(planeXY[i+1][j+1]), &(planeXY[i][j+1]), scalar );
			}
		}
	}	
}

void PointField::reset() {
	// Set the range sliders appropriately
	isoValue = (SCALAR_HIGH - SCALAR_LOW) / 2;
	xRange[0] = yRange[0] = zRange[0] = BOUNDS_LOW;
	xRange[1] = yRange[1] = zRange[1] = BOUNDS_HIGH;
	tRange[0] = SCALAR_LOW;
	tRange[1] = SCALAR_HIGH;
	
	
	// Set the boolean options to defaults
	numContours = 12;
	numSlices = 12;
	contourYZ = contourXY = contourXZ = 0;
	points = 0;
	isoSurface = 0;	
	jitter = 0;

	// Re-seed the random number generator
	srandom( time(NULL) );
	
	displayListCurrent = false;
		textureCurrent = false;
}

void PointField::checkboxCallback( int id ) {
	displayListCurrent = false;
//		textureCurrent = false;
}

void PointField::sliderCallback( int id ) {
//	char str[32];

	displayListCurrent = false;
//		textureCurrent = false;
/*
	switch( id ) {
		case XRANGE:
			sprintf( str, "X Range: %f to %f", xRange[0], xRange[1] );
			xRangeLabel->set_text( str );
			break;
		case YRANGE:
			sprintf( str, "Y Range: %f to %f", yRange[0], yRange[1] );
			yRangeLabel->set_text( str );
			break;
		case ZRANGE:;
			sprintf( str, "Z Range: %f to %f", zRange[0], zRange[1] );
			zRangeLabel->set_text( str );
			break;
//		case RRANGE:
//			sprintf( str, "Radial Dist.: %f to %f", rRange[0], rRange[1] );
//			rRangeLabel->set_text( str );
//			break;
		case TRANGE:
			sprintf( str, "Temp Range: %f to %f", tRange[0], tRange[1] );
			tRangeLabel->set_text( str );
			break;
	}*/
}
