/*
 *  PointField.h
 *  VolumeRender
 *
 *  Created by William Dillon on 2/1/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include "Utility.h"
#include "Model.h"

#ifndef POINTFIELD_H
#define POINTFIELD_H

//#define UINT

class PointField : public Model {
public:
	// Constructor
	PointField( int newWidth, int newHeight, int newDepth );
	~PointField();

	// UI Creation
	void initUI( GLUI *glui );

	// Callbacks
	void sliderCallback( int id );
	void checkboxCallback( int id );
	void draw();
	void reset();

	int width, height, depth;
	float getData( int u, int v, int w );
	// Note that this form of getData does not used cached values
	float getData( float u, float v, float w );	
private:
	// Functions to create and manage the dataset
	void computeData();
	void setData( int u, int v, int w, float s );
	
	// Parameters for point cloud
	int points, jitter;
	float xRange[2], yRange[2], zRange[2], tRange[2];
	// Parameters for volume slices
	int sliceXY, sliceXZ, sliceYZ;
	float xSlice, ySlice, zSlice;
	// Parameters for contour planes
	int numContours;
	int contourXY, contourXZ, contourYZ;
	float xContour, yContour, zContour;
	int isoSurface;
	int numSlices;
	float isoValue;
	void drawContour( int plane, float scalar, float x, float y, float z );

	// State variables pertaining to and the data
	GLuint texID;
#ifdef UINT
	unsigned char *data;
#else
	float *data;
#endif
	bool textureCurrent;
	void refreshTexture( void );
	struct xyzs  planeXY[NUMNODES][NUMNODES], planeXZ[NUMNODES][NUMNODES], planeYZ[NUMNODES][NUMNODES];

	// Use display lists whenever possible
	int volumeDisplayList;
	bool displayListCurrent;
	void internalDraw( void );
	
	// References to GLUI elements
	GLUI *glui;
	GLUI_StaticText *xRangeLabel;
	GLUI_StaticText *yRangeLabel;
	GLUI_StaticText *zRangeLabel;
	GLUI_StaticText *tRangeLabel;	
};
#endif