/*
 *  3dScatter.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 2/24/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "Scatter3D.h"
#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

// Deal with the GLUI callback structure
static Scatter3D *self;
void GLUISliderCallback( int id ) { return; }
void GLUICheckboxCallback( int id) { self->checkboxCallback( id ); }

// Constructor
Scatter3D::Scatter3D( char *fileName, int num ) {
	int i, j;
	ifstream dataFile;
	
	// Perform some initialization
	numPoints = num;
	self = this;
	
	// Attempt to open datafile
	cout << "Attempting to load data from file: " << fileName << "." << endl;
	dataFile.open( fileName );
	if( !dataFile.is_open() ) {
		cout << "Unable to open file." << endl;
		return;
	}

	// First iterator, one loop per datafile line
	for( i = 0; i < numPoints; i++ ) {

		// Read the new line
		if( dataFile.eof() ) {
			cout << "Incomplete splats file." << endl;
			return;
		}

		// Second iterator, one loop per element
		for( j = 0; j < 4; j++ ) {
			dataFile >> splats[i][j];
		}
		cout << endl;
	}
	
	// We're done here, close the file
	displayListCurrent = false;
	
	// Compute the surface
	for( i = 0; i < DATADENSITY; i++ ) {
		for( j = 0; j < DATADENSITY; j++ ) {
			surface[i][j] = surfacePoint( modelSpaceCoord( i, DATADENSITY, 0.0, 1.0),
										  modelSpaceCoord( j, DATADENSITY, 0.0, 1.0));
		}
	}
	
	dataFile.close();
}

double POW4( double in ) { return in*in*in*in; }

float
Scatter3D::surfacePoint( float x, float y ) {
	double top1 = 1.0 - 2.0*x;
	double top2 = 1.0 - 2.0*y;
	double bottom1 = 2.0*(1.0 - x)*y;
	double bottom2 = 2.0*(1.0 - y)*x;
	double ptBar1 = sqrt( top1/bottom1 );
	double ptBar2 = sqrt( top2/bottom2 );
	double pt1 = 1.0 - POW4(1.0 - ptBar1);
	double pt2 = 1.0 - POW4(1.0 - ptBar2);
	return (pt1 + pt2) / 2;
}

// GLUI callback ID's
#define DISPLAY 1

// Create the GLUI widgets necessary for this model
void Scatter3D::initUI( GLUI *glui ) {
	GLUI_Scrollbar *slider;
	
	new GLUI_Checkbox( glui, "Draw simulation points", &drawSplats, DISPLAY, GLUICheckboxCallback );
	new GLUI_Checkbox( glui, "Draw solution surface", &drawSurface, DISPLAY, GLUICheckboxCallback );
	
	slider = new GLUI_Scrollbar( glui, "Opacity", GLUI_SCROLL_HORIZONTAL, &surfaceOpacity, DISPLAY, GLUICheckboxCallback );
	slider->set_float_limits( 0.0, 1.0 );
	new GLUI_StaticText( glui, "Surface Opacity" );
	
	displayList = glGenLists( 1 );
	glColor3f( 0.0, 0.0, 0.0 );
	cubeList = generateCubeList();
	return;
}

void Scatter3D::checkboxCallback( int id ) {
	if( id == DISPLAY ) {
		displayListCurrent = false;
	}
}

void Scatter3D::draw() {
	if( !displayListCurrent )
		internalDraw();
	else
		glCallList( displayList );
	
	return;
}

inline void doColorSurface( float inVal, float opacity ) {
	float hsv[3] = { 0.0, 1.0, 1.0 };
	float rgb[4] = { 1.0, 1.0, 1.0, 1.0 };
	
	hsv[0] = inVal * 240;
	HSV2RGB( hsv, rgb );
	rgb[3] = opacity;
	glColor4fv( rgb );	
}

void Scatter3D::internalDraw() {
	int i, j;
	
	glNewList( displayList, GL_COMPILE_AND_EXECUTE );

	// The problem space for this model is 0 to 1 for all 3 axes
	glPushMatrix();
	glScalef( 0.25, 0.25, 0.5 );
	glTranslatef( 1.0, 1.0, 1.0);
	glCallList( cubeList );
	glPopMatrix();
	
	glPointSize( 2.0 );
	
	// Draw the points for all available simulaton runs
	if( drawSplats == GLUI_TRUE ) {
		glColor3f( 0.0, 0.0, 0.0 );
		for( i = 0; i < numPoints; i++ ) {
			glBegin( GL_POINTS ); {
			// Display the points of simulation runs only if the result is >= 50%
				if( splats[i][3] > 0.0 && splats[i][3] < 1.0 )
				// Make a point at the 3D location of the simulation run
					glVertex3f( splats[i][0], splats[i][1], splats[i][2] );
			} glEnd();		
		}		
	}
	
	// Draw the surface of the solution
	if( drawSurface == GLUI_TRUE ) {
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		glEnable( GL_BLEND );
		glBegin( GL_TRIANGLES ); {
		for( i = 0; i < DATADENSITY; i++ ) {
				for( j = 0; j < DATADENSITY; j++ ) {
					// Draw the surface as a combination of triangles if atleast one corner is in bounds
					if(   surface[i][j]   <= 1 &&   surface[i][j]   >= 0 &&
						 surface[i][j+1]  <= 1 &&  surface[i][j+1]  >= 0 &&
						 surface[i+1][j]  <= 1 &&  surface[i+1][j]  >= 0 &&
						surface[i+1][j+1] <= 1 && surface[i+1][j+1] >= 0 ) {
						doColorSurface( surface[i][j], surfaceOpacity );
						glVertex3f( modelSpaceCoord( i,   DATADENSITY, 0.0, 1.0 ),
									modelSpaceCoord( j,   DATADENSITY, 0.0, 1.0 ), surface[i][j] );
						doColorSurface( surface[i+1][j], surfaceOpacity );
						glVertex3f( modelSpaceCoord( i+1, DATADENSITY, 0.0, 1.0 ),
									modelSpaceCoord( j,   DATADENSITY, 0.0, 1.0 ), surface[i+1][j] );
						doColorSurface( surface[i][j+1], surfaceOpacity );
						glVertex3f( modelSpaceCoord( i,   DATADENSITY, 0.0, 1.0 ),
									modelSpaceCoord( j+1, DATADENSITY, 0.0, 1.0 ), surface[i][j+1] );
						doColorSurface( surface[i+1][j], surfaceOpacity );
						glVertex3f( modelSpaceCoord( i+1, DATADENSITY, 0.0, 1.0 ),
									modelSpaceCoord( j,   DATADENSITY, 0.0, 1.0 ), surface[i+1][j] );
						doColorSurface( surface[i][j+1], surfaceOpacity );
						glVertex3f( modelSpaceCoord( i,   DATADENSITY, 0.0, 1.0 ),
									modelSpaceCoord( j+1, DATADENSITY, 0.0, 1.0 ), surface[i][j+1] );
						doColorSurface( surface[i+1][j+1], surfaceOpacity );
						glVertex3f( modelSpaceCoord( i+1, DATADENSITY, 0.0, 1.0 ),
									modelSpaceCoord( j+1, DATADENSITY, 0.0, 1.0 ), surface[i+1][j+1] );						
					}
				}			
			}
		} glEnd();
		glDisable( GL_BLEND );
	}
	
	// Finished drawing
	glEndList();
	displayListCurrent = true;
	return;
}
