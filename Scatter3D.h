/*
 *  Scatter3D.h
 *  VolumeRender
 *
 *  Created by William Dillon on 2/24/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include "Utility.h"
#include "Model.h"

#ifndef SCATTER3D_H
#define SCATTER3D_H

#define DATADENSITY 500

class Scatter3D : public Model {
public:
	// Constructor
	Scatter3D( char *fileName, int num );
	
	// UI Creation
	void initUI( GLUI *glui );
	
	// Callbacks
	void sliderCallback( int id );
	void checkboxCallback( int id );
	void draw();
//	void reset();
	
	float getData( float u, float v, float w );	
private:
	// Drawing list routines and data
	void internalDraw();
	bool displayListCurrent;
	int  displayList;
	int  cubeList;
	
	// Scatter plot points
	int numPoints;
	int drawSplats;
	float splats[DATADENSITY][4];
	// Datapoints and density of surface
	int drawSurface;
	float surfaceOpacity;
	float surfacePoint( float x, float y );
	float surface[DATADENSITY][DATADENSITY];
};
#endif