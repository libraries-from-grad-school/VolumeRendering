/*
 *  TerrainVis.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 3/5/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "TerrainVis.h"
#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

// Properties of the datafile
const float LNGMIN = { -289.6   };
const float LNGMAX = {  289.6   };
const float LATMIN = { -197.5   };
const float LATMAX = {  211.2   };
const float HGTMIN = {    0.0   };
const float HGTMAX = {    2.891 };

// Deal with the GLUI callback structure
static TerrainVis *self;
void GLUISliderCallback( int id ) { return; }
void GLUICheckboxCallback( int id) { self->checkboxCallback( id ); }

// Constructor
TerrainVis::TerrainVis( char *heightFieldFile, char *satImageFile ) {
	int i, j;
	float dLat, dLng, lat, lng;
	ifstream dataFile;
	
	// Perform some initialization
	self = this;
	
	// Load satellite image into texture
	textureImage = BmpToTexture( satImageFile, &textureWidth, &textureHeight );
	if( textureWidth != 256 || textureHeight != 256 || textureImage == NULL ) {
		cout << "Error while loading image file" << endl;
//		return;
	}
	
	// Attempt to open datafile
	cout << "Attempting to load data from file: " << heightFieldFile << "." << endl;
	dataFile.open( heightFieldFile );
	if( !dataFile.is_open() ) {
		cout << "Unable to open file." << endl;
		return;
	}

	dLat = (LATMAX - LATMIN) / (float)NUMLATS;
	dLng = (LNGMAX - LNGMIN) / (float)NUMLNGS;
	
	// First iterator, one loop per datafile line
	for( i = 0, lat = LATMAX; i < NUMLATS; i++, lat -= dLat ) {
		
		// Make sure we haven't run out of file
		if( dataFile.eof() ) {
			cout << "Incomplete hight file." << endl;
			return;
		}
		
		// Second iterator, one loop per element
		for( j = 0, lng = LNGMIN; j < NUMLNGS; j++, lng += dLng ) {
			Points[i][j].lng = lng;
			Points[i][j].lat = lat;
			dataFile >> Points[i][j].hgt;
			Points[i][j].s = ( lng - LNGMIN ) / ( LNGMAX - LNGMIN );
			Points[i][j].t = ( lat - LATMIN ) / ( LATMAX - LATMIN );
			Points[i][j].t = Points[i][j].t * ( 183. / 255. );
			
			
		}
	}
	
	for( i = 0; i < NUMLNGS - 1; i++ )
		for( j = 0; j < NUMLATS - 1; j++ ) {
			
		}

	// We're done here, close the file
	dataFile.close();
	displayListCurrent = false;
	
	this->reset();
}

void TerrainVis::reset() {
	linear = 1;
	modulate = 1;
	
	iMultiply = 15.0;
	zMultiply = 10.0;
}
// GLUI callback ID's
#define TEXTURE 0
#define DISPLAY 1

// Create the GLUI widgets necessary for this model
void TerrainVis::initUI( GLUI *glui ) {
	GLUI_Scrollbar *slider;
	
	new GLUI_Checkbox( glui, "Modulate texture with surface", &modulate, TEXTURE, GLUICheckboxCallback );
	new GLUI_Checkbox( glui, "Use linear texture filtering",  &linear,   TEXTURE, GLUICheckboxCallback );
//	new GLUI_Checkbox( glui, "Ride the cloud",                &ride,     DISPLAY, GLUICheckboxCallback );
	
	slider = new GLUI_Scrollbar( glui, "Height Exageration", GLUI_SCROLL_HORIZONTAL, &zMultiply, DISPLAY, GLUICheckboxCallback );
	slider->set_float_limits( 1.0, 50.0 );
	new GLUI_StaticText( glui, "Height Exageration" );

	slider = new GLUI_Scrollbar( glui, "Illumination Exageration", GLUI_SCROLL_HORIZONTAL, &iMultiply, DISPLAY, GLUICheckboxCallback );
	slider->set_float_limits( 1.0, 50.0 );
	new GLUI_StaticText( glui, "Illumination Exageration" );
	
	cloud = gluNewQuadric();
	displayList = glGenLists( 1 );
	glGenTextures( 1, &texID );
	glColor3f( 0.0, 0.0, 0.0 );
	return;
}

void TerrainVis::checkboxCallback( int id ) {
	switch( id ) {
		case DISPLAY:
			displayListCurrent = false;
			break;
		case TEXTURE:
			textureCurrent = false;
			break;
		default:
			displayListCurrent = false;
			textureCurrent = false;
	}
}

void TerrainVis::draw() {
	if( !textureCurrent )
		refreshTexture();
	
	if( !displayListCurrent )
		internalDraw();
	else
		glCallList( displayList );
	
	return;
}

// Reload the texture to the GPU
void TerrainVis::refreshTexture( void ) {
	
	glBindTexture( GL_TEXTURE_2D, texID );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
	glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

	if( linear ) {
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	} else {
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);		
	}
	
	if( modulate )
		glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	else
		glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_STORAGE_HINT_APPLE, GL_STORAGE_CACHED_APPLE );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, textureImage );
	textureCurrent = true;
}

void TerrainVis::internalDraw() {
	int i, j;
	float scaleFactor = 1.0/(LNGMAX-LNGMIN);
	float v1[3], v2[3], n[3], inten;
	float cloudX, cloudY;
	
	glNewList( displayList, GL_COMPILE_AND_EXECUTE );

	glPushMatrix();
	glScalef( -1*scaleFactor, scaleFactor, scaleFactor );
	
	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, texID );
	
	glBegin( GL_TRIANGLES ); {
		for( i = 0; i < NUMLATS - 1; i++ ) {
			for( j = 0; j < NUMLNGS - 1; j++ ) {
				// Find the vector along edge 1->0
				v1[0] = Points[ i ][ j ].lat - Points[i+1][ j ].lat;
				v1[1] = Points[ i ][ j ].lng - Points[i+1][ j ].lng;
				v1[2] = Points[ i ][ j ].hgt - Points[i+1][ j ].hgt;
				// Find the vector along edge 1->3
				v2[0] = Points[i+1][j+1].lat - Points[i+1][ j ].lat;
				v2[1] = Points[i+1][j+1].lng - Points[i+1][ j ].lng;
				v2[2] = Points[i+1][j+1].hgt - Points[i+1][ j ].hgt;
				// Apply the intensity magnification
				v1[2] = v1[2] * iMultiply;
				v2[2] = v2[2] * iMultiply;
				// Find the vector normal to the plane defined by v1 and v2
				cross( v1, v2, n );
				// Normallize the y component of the normal vector and call it 'intensity'
				inten = n[2] / sqrt( n[0]*n[0]  +  n[1]*n[1]  +  n[2]*n[2] );
				glColor3f( inten, inten, inten );
				// Draw a triangle between points 0, 1, and 3
				glTexCoord2d( Points[ i ][ j ].s,   Points[ i ][ j ].t );
				glVertex3f  ( Points[ i ][ j ].lat, Points[ i ][ j ].lng, Points[ i ][ j ].hgt * zMultiply );	// 0
				glTexCoord2d( Points[i+1][ j ].s,   Points[i+1][ j ].t );
				glVertex3f  ( Points[i+1][ j ].lat, Points[i+1][ j ].lng, Points[i+1][ j ].hgt * zMultiply );	// 1
				glTexCoord2d( Points[i+1][j+1].s,   Points[i+1][j+1].t );
				glVertex3f  ( Points[i+1][j+1].lat, Points[i+1][j+1].lng, Points[i+1][j+1].hgt * zMultiply );	// 3

				// Find the vector along edge 2->3
				v1[0] = Points[i+1][j+1].lat - Points[ i ][j+1].lat;
				v1[1] = Points[i+1][j+1].lng - Points[ i ][j+1].lng;
				v1[2] = Points[i+1][j+1].hgt - Points[ i ][j+1].hgt;
				// Find the vector along edge 2->0
				v2[0] = Points[ i ][ j ].lat - Points[ i ][j+1].lat;
				v2[1] = Points[ i ][ j ].lng - Points[ i ][j+1].lng;
				v2[2] = Points[ i ][ j ].hgt - Points[ i ][j+1].hgt;
				// Apply the intensity magnification
				v1[2] = v1[2] * iMultiply;
				v2[2] = v2[2] * iMultiply;
				// Find the vector normal to the plane defined by v1 and v2
				cross( v1, v2, n );
				// Normallize the z component of the normal vector and call it 'intensity'
				inten = n[2] / sqrt( n[0]*n[0]  +  n[1]*n[1]  +  n[2]*n[2] );
				glColor3f( inten, inten, inten );
				// Draw a triangle between points 0, 3 and 2
				glTexCoord2d( Points[ i ][ j ].s,   Points[ i ][ j ].t );
				glVertex3f  ( Points[ i ][ j ].lat, Points[ i ][ j ].lng, Points[ i ][ j ].hgt * zMultiply );	// 0
				glTexCoord2d( Points[i+1][j+1].s,   Points[i+1][j+1].t );
				glVertex3f  ( Points[i+1][j+1].lat, Points[i+1][j+1].lng, Points[i+1][j+1].hgt * zMultiply );	// 3
				glTexCoord2d( Points[ i ][j+1].s,   Points[ i ][j+1].t );
				glVertex3f  ( Points[ i ][j+1].lat, Points[ i ][j+1].lng, Points[ i ][j+1].hgt * zMultiply );	// 2				
			}
		}			
	} glEnd();
	glDisable( GL_TEXTURE_2D );
	
	// Enable blending for the gas cloud
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glEnable( GL_BLEND );	
	// Draw the cloud disk
	cloudX = 130 * sin( currentTime * 2*M_PI );
	cloudY = 130 * cos( currentTime * 2*M_PI );
	glTranslatef( cloudX, cloudY, 100);
	gluQuadricOrientation( cloud, GLU_OUTSIDE );
	gluQuadricDrawStyle( cloud, GLU_FILL );
	glColor4f( 0.2, 0.8, 0.5, 0.4 );
	gluDisk( cloud, 0, 50 , 20, 1);
	glDisable( GL_BLEND );
	
	// Finished drawing
	glPopMatrix();
	glEndList();
	displayListCurrent = true;
	return;
}

void TerrainVis::keyboard( char k, int m ) {

	if( k == 'p' ) {
		if( paused )
			paused = false;
		else
			paused = true;
	}
}

void TerrainVis::animate() {
	// Compute cycle time
	if( !paused ) {
		currentTime = float( glutGet( GLUT_ELAPSED_TIME ) % (int)(10.0 * 1000)) / (10.0 * 1000);
		// Force re-draw
		displayListCurrent = false;
		glutPostRedisplay();		
	}
}
