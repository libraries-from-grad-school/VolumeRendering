/*
 *  TerrainVis.h
 *  VolumeRender
 *
 *  Created by William Dillon on 3/5/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include "Utility.h"
#include "Model.h"

#ifndef TERRAINVIS_H
#define TERRAINVIS_H

// Size of terrain data
#define NUMLNGS		201
#define NUMLATS		105

// Data structure to store heightfield
struct LngLatHgt
{
	float lng, lat, hgt, s, t;
};

class TerrainVis : public Model {
public:
	// Constructor
	TerrainVis( char *terrainFileName, char *satImageFileName );
	
	// UI Creation
	void initUI( GLUI *glui );
	
	// Callbacks
	void sliderCallback( int id );
	void checkboxCallback( int id );
	void draw();
	void reset();
	void animate();
	void keyboard( char k, int m );
	
	float getData( float u, float v, float w );	
private:
	// Drawing list routines and data
	void internalDraw();
	bool displayListCurrent;
	int  displayList;
	// Texture routines and data
	void refreshTexture( void );
	bool textureCurrent;
	int  modulate;
	int  linear;
	GLuint texID;

	bool paused;
	float currentTime;
	
	// Quadric data
	GLUquadric *cloud;
	// Terrain data
	float zMultiply;
	float iMultiply;
	struct LngLatHgt Points[NUMLATS][NUMLNGS];
	unsigned char *textureImage;
	int textureWidth, textureHeight;
};
#endif