/*
 *  Utility.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 1/30/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Utility.h"
#include "GLUT/glut.h"
#include "glui.h"

// fraction of the length to use as height of the characters:
#define LENFRAC		0.10	
// fraction of length to use as start location of the characters:
#define BASEFRAC	1.10

const GLfloat AXES_COLOR[] = { 1., .5, 0. };
#define AXES_WIDTH	1.5
#define LINE_WIDTH	1.0

// the stroke characters 'X' 'Y' 'Z' :

static float xx[]	= { 0.0, 1.0,  0.0,  1.0 };
static float xy[]	= { -.5, 0.5,  0.5, -0.5 };
static float yx[]	= { 0.0, 0.0, -0.5,  0.5 };
static float yy[]	= { 0.0, 0.6,  1.0,  1.0 };
static float zx[]	= { 1.0, 0.0,  1.0,  0.0, 0.25, 0.75 };
static float zy[]	= { 0.5, 0.5, -0.5, -0.5, 0.00, 0.00 };
static int xorder[] = {   1,   2,   -3,  4 };
static int yorder[] = {   1,   2,    3, -2,  4 };
static int zorder[] = {   1,   2,    3,  4, -5, 6};

// Convert a scalar and it's range to RGB using the rainbow scale
void scalar2RGB( float scalar, float scalar_high, float scalar_low, float RGB[3] ) {
	float HSV[3] = { 0.0, 1.0, 1.0 };
	HSV[0] = 240 - (240 * ( scalar / scalar_high - scalar_low ));
	HSV2RGB( HSV, RGB );
}

// Generate a random offset from -1 to 1
float randomOffset() {
	float temp = random() % UINT_MAX;	// Constrain the random number
	temp = temp / UINT_MAX;				// Normallize the number
	temp = temp * 2.0;					// Scale it to the parameter space
	temp = temp - 1.0;					// Translate is to parameter space
	
	return temp;
}

float SQR( float value ) {
	return value * value;
}

float cube( float value ) {
	return value * value * value;
}

void 
CheckGlErrors( const char* caller ) 
{ 
	unsigned int glerr= glGetError(); 
	if( glerr== GL_NO_ERROR ) 
		return; 
	fprintf( stderr, "GL Error discovered from caller ‘%s‘: ", caller ); 
	switch( glerr) 
	{ 
		case GL_INVALID_ENUM: 
			fprintf( stderr, "Invalid enum.\n" ); 
			break; 
		case GL_INVALID_VALUE: 
			fprintf( stderr, "Invalid value.\n" ); 
			break; 
		case GL_INVALID_OPERATION: 
			fprintf( stderr, "Invalid Operation.\n" ); 
			break; 
		case GL_STACK_OVERFLOW: 
			fprintf( stderr, "Stack overflow.\n" ); 
			break; 
		case GL_STACK_UNDERFLOW: 
			fprintf(stderr, "Stack underflow.\n" ); 
			break; 
		case GL_OUT_OF_MEMORY: 
			fprintf( stderr, "Out of memory.\n" ); 
			break; 
		default: 
			fprintf( stderr, "Unknown OpenGL error: %d (0x%0x)\n", glerr, glerr); 
	} 
} 

// Convert HSV to RGB values (from Dr. Metoyer's skeleton code)
void HSV2RGB(float hsv[3], float rgb[3]) {
	float tmp1 = hsv[2] * (1-hsv[1]);
	float tmp2 = hsv[2] * (1-hsv[1] * (hsv[0] / 60.0f - (int) (hsv[0]/60.0f) ));
	float tmp3 = hsv[2] * (1-hsv[1] * (1 - (hsv[0] / 60.0f - (int) (hsv[0]/60.0f) )));
	switch((int)(hsv[0] / 60)) {
		case 0:
			rgb[0] = hsv[2] ;
			rgb[1] = tmp3 ;
			rgb[2] = tmp1 ;
			break;
		case 1:
			rgb[0] = tmp2 ;
			rgb[1] = hsv[2] ;
			rgb[2] = tmp1 ;
			break;
		case 2:
			rgb[0] = tmp1 ;
			rgb[1] = hsv[2] ;
			rgb[2] = tmp3 ;
			break;
		case 3:
			rgb[0] = tmp1 ;
			rgb[1] = tmp2 ;
			rgb[2] = hsv[2] ;
			break;
		case 4:
			rgb[0] = tmp3 ;
			rgb[1] = tmp1 ;
			rgb[2] = hsv[2] ;
			break;
		case 5:
			rgb[0] = hsv[2] ;
			rgb[1] = tmp1 ;
			rgb[2] = tmp2 ;
			break;
	}	
}

void
Axes( float length )
{
	int i, j;			// counters
	float fact;			// character scale factor
	float base;			// character start location
	
	
	glBegin( GL_LINE_STRIP );
	glVertex3f( length, 0., 0. );
	glVertex3f( 0., 0., 0. );
	glVertex3f( 0., length, 0. );
	glEnd();
	glBegin( GL_LINE_STRIP );
	glVertex3f( 0., 0., 0. );
	glVertex3f( 0., 0., length );
	glEnd();
	
	fact = LENFRAC * length;
	base = BASEFRAC * length;
	
	glBegin( GL_LINE_STRIP );
	for( i = 0; i < 4; i++ )
	{
		j = xorder[i];
		if( j < 0 )
		{
			
			glEnd();
			glBegin( GL_LINE_STRIP );
			j = -j;
		}
		j--;
		glVertex3f( base + fact*xx[j], fact*xy[j], 0.0 );
	}
	glEnd();
	
	glBegin( GL_LINE_STRIP );
	for( i = 0; i < 5; i++ )
	{
		j = yorder[i];
		if( j < 0 )
		{
			
			glEnd();
			glBegin( GL_LINE_STRIP );
			j = -j;
		}
		j--;
		glVertex3f( fact*yx[j], base + fact*yy[j], 0.0 );
	}
	glEnd();
	
	glBegin( GL_LINE_STRIP );
	for( i = 0; i < 6; i++ )
	{
		j = zorder[i];
		if( j < 0 )
		{
			
			glEnd();
			glBegin( GL_LINE_STRIP );
			j = -j;
		}
		j--;
		glVertex3f( 0.0, fact*zy[j], base + fact*zx[j] );
	}
	glEnd();
	
}

int generateCubeList() {
	int listID = glGenLists( 1 );
	
	glNewList( listID, GL_COMPILE ); {
		glBegin( GL_LINES ); {
			// Back face of cube
			glVertex3i( -1, -1, -1 );
			glVertex3i( -1,  1, -1 );
			glVertex3i( -1,  1, -1 );
			glVertex3i(  1,  1, -1 );
			glVertex3i(  1,  1, -1 );
			glVertex3i(  1, -1, -1 );
			glVertex3i(  1, -1, -1 );
			glVertex3i( -1, -1, -1 );
			// Front face of cube
			glVertex3i( -1, -1,  1 );
			glVertex3i( -1,  1,  1 );
			glVertex3i( -1,  1,  1 );
			glVertex3i(  1,  1,  1 );
			glVertex3i(  1,  1,  1 );
			glVertex3i(  1, -1,  1 );
			glVertex3i(  1, -1,  1 );
			glVertex3i( -1, -1,  1 );
			// Connecting lines
			glVertex3i( -1, -1, -1 );
			glVertex3i( -1, -1,  1 );
			glVertex3i( -1,  1, -1 );
			glVertex3i( -1,  1,  1 );
			glVertex3i(  1,  1, -1 );
			glVertex3i(  1,  1,  1 );
			glVertex3i(  1, -1, -1 );
			glVertex3i(  1, -1,  1 );
		} glEnd();
	} glEndList();
	
	return listID;
}

int generateAxesList() {
	int AxesList = glGenLists( 1 );
	
	glNewList( AxesList, GL_COMPILE ); {
		glColor3fv( AXES_COLOR );
		glLineWidth( AXES_WIDTH );
		
		Axes( 1.5 );
		glLineWidth( LINE_WIDTH );
	} glEndList();
	
	return AxesList;
}

void contourQuad( struct xyzs *p0, struct xyzs *p1, struct xyzs *p2, struct xyzs *p3, float scalar ) {
	float deltaS, deltaX, deltaY, deltaZ, temp;
	int numCrossings = 0;
	
	if( p0->s > scalar && p1->s < scalar ||
		p0->s < scalar && p1->s > scalar ) {
		numCrossings++;
		deltaX = p1->x - p0->x;
		deltaY = p1->y - p0->y;
		deltaZ = p1->z - p0->z;
		deltaS = p1->s - p0->s;
		
		temp = scalar - p0->s;
		temp = temp / deltaS;
		
		glVertex3f( p0->x + deltaX * temp,
					p0->y + deltaY * temp,
					p0->z + deltaZ * temp );
	}
	if( p1->s > scalar && p2->s < scalar ||
		p1->s < scalar && p2->s > scalar ) {
		numCrossings++;
		deltaX = p2->x - p1->x;
		deltaY = p2->y - p1->y;
		deltaZ = p2->z - p1->z;
		deltaS = p2->s - p1->s;
		
		temp = scalar - p1->s;
		temp = temp / deltaS;
		
		glVertex3f( p1->x + deltaX * temp,
					p1->y + deltaY * temp,
					p1->z + deltaZ * temp );
	}
	if( p2->s > scalar && p3->s < scalar ||
		p2->s < scalar && p3->s > scalar ) {
		numCrossings++;
		deltaX = p3->x - p2->x;
		deltaY = p3->y - p2->y;
		deltaZ = p3->z - p2->z;
		deltaS = p3->s - p2->s;
		
		temp = scalar - p2->s;
		temp = temp / deltaS;
		
		glVertex3f( p2->x + deltaX * temp,
					p2->y + deltaY * temp,
					p2->z + deltaZ * temp );
	}
	if( p3->s > scalar && p0->s < scalar ||
		p3->s < scalar && p0->s > scalar ) {
		numCrossings++;
		deltaX = p0->x - p3->x;
		deltaY = p0->y - p3->y;
		deltaZ = p0->z - p3->z;
		deltaS = p0->s - p3->s;
		
		temp = scalar - p3->s;
		temp = temp / deltaS;
		
		glVertex3f( p3->x + deltaX * temp,
					p3->y + deltaY * temp,
					p3->z + deltaZ * temp );
	}
	
	if( numCrossings == 1 ||
		numCrossings == 3 ) {
		fprintf( stderr, "Odd number of crossings, trying to deal with it.\n" );
		glVertex3f( p0->x, p0->y, p0->z );
	}
	
}

float modelSpaceCoord( float i, float maxDim, float bounds_low, float bounds_high ) {
	float retval;
	retval = bounds_low + (( bounds_high - bounds_low ) * ( i / maxDim));
	return retval;
}

int dataSpaceCoord( float i, float maxDim, float bounds_low, float bounds_high ) {
	int retval;
	retval = (int)( (i - bounds_low) / (bounds_high - bounds_low) * maxDim );
	return retval;
}

// size of wings as fraction of length:
#define WINGS	0.10
// Axes
#define X	1
#define Y	2
#define Z	3

static float axx[3] = { 1., 0., 0. };
static float ayy[3] = { 0., 1., 0. };
static float azz[3] = { 0., 0., 1. };

void
Arrow( float tail[3], float head[3] )
{
	float u[3], v[3], w[3];		/* arrow coordinate system		*/
	float d;					/* wing distance				*/
	float x, y, z;				/* point to plot				*/
	float mag;					/* magnitude of major direction	*/
	float f;					/* fabs of magnitude			*/
	int axis;					/* which axis is the major		*/
	
	
	/* set w direction in u-v-w coordinate system:				*/
	
	w[0] = head[0] - tail[0];
	w[1] = head[1] - tail[1];
	w[2] = head[2] - tail[2];
	
	
	/* determine major direction:								*/
	
	axis = X;
	mag = fabs( w[0] );
	if( (f=fabs(w[1]))  > mag )
	{
		axis = Y;
		mag = f;
	}
	if( (f=fabs(w[2]))  > mag )
	{
		axis = Z;
		mag = f;
	}
	
	
	/* set size of wings and turn w into a unit vector:		*/
	
	d = WINGS * unit( w, w );
	
	
	/* draw the shaft of the arrow:					*/
	
	glBegin( GL_LINE_STRIP );
	glVertex3fv( tail );
	glVertex3fv( head );
	glEnd();
	
	/* draw two sets of wings in the non-major directions:		*/
	
	if( axis != X )
	{
		cross( w, axx, v );
		(void) unit( v, v );
		cross( v, w, u  );
		x = head[0] + d * ( u[0] - w[0] );
		y = head[1] + d * ( u[1] - w[1] );
		z = head[2] + d * ( u[2] - w[2] );
		glBegin( GL_LINE_STRIP );
		glVertex3fv( head );
		glVertex3f( x, y, z );
		glEnd();
		x = head[0] + d * ( -u[0] - w[0] );
		y = head[1] + d * ( -u[1] - w[1] );
		z = head[2] + d * ( -u[2] - w[2] );
		glBegin( GL_LINE_STRIP );
		glVertex3fv( head );
		glVertex3f( x, y, z );
		glEnd();
	}
	
	
	if( axis != Y )
	{
		cross( w, ayy, v );
		(void) unit( v, v );
		cross( v, w, u  );
		x = head[0] + d * ( u[0] - w[0] );
		y = head[1] + d * ( u[1] - w[1] );
		z = head[2] + d * ( u[2] - w[2] );
		glBegin( GL_LINE_STRIP );
		glVertex3fv( head );
		glVertex3f( x, y, z );
		glEnd();
		x = head[0] + d * ( -u[0] - w[0] );
		y = head[1] + d * ( -u[1] - w[1] );
		z = head[2] + d * ( -u[2] - w[2] );
		glBegin( GL_LINE_STRIP );
		glVertex3fv( head );
		glVertex3f( x, y, z );
		glEnd();
	}
	
	
	
	if( axis != Z )
	{
		cross( w, azz, v );
		(void) unit( v, v );
		cross( v, w, u  );
		x = head[0] + d * ( u[0] - w[0] );
		y = head[1] + d * ( u[1] - w[1] );
		z = head[2] + d * ( u[2] - w[2] );
		glBegin( GL_LINE_STRIP );
		glVertex3fv( head );
		glVertex3f( x, y, z );
		glEnd();
		x = head[0] + d * ( -u[0] - w[0] );
		y = head[1] + d * ( -u[1] - w[1] );
		z = head[2] + d * ( -u[2] - w[2] );
		glBegin( GL_LINE_STRIP );
		glVertex3fv( head );
		glVertex3f( x, y, z );
		glEnd();
	}
	
	
	/* done:							*/
	
}



float
dot( float v1[3], float v2[3] )
{
	return( v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2] );
}



void
cross( float v1[3], float v2[3], float vout[3] )
{
	float tmp[3];
	
	tmp[0] = v1[1]*v2[2] - v2[1]*v1[2];
	tmp[1] = v2[0]*v1[2] - v1[0]*v2[2];
	tmp[2] = v1[0]*v2[1] - v2[0]*v1[1];
	
	vout[0] = tmp[0];
	vout[1] = tmp[1];
	vout[2] = tmp[2];
}



float
unit( float vin[3], float vout[3] )
{
	float dist, f ;
	
	dist = vin[0]*vin[0] + vin[1]*vin[1] + vin[2]*vin[2];
	
	if( dist > 0.0 )
	{
		dist = sqrt( dist );
		f = 1. / dist;
		vout[0] = f * vin[0];
		vout[1] = f * vin[1];
		vout[2] = f * vin[2];
	}
	else
	{
		vout[0] = vin[0];
		vout[1] = vin[1];
		vout[2] = vin[2];
	}
	
	return( dist );
}

float
magnitude( float vin[3] )
{
	return sqrt( (vin[1] * vin[1]) + (vin[2] * vin[2]) + (vin[3] * vin[3]) );
}

/*****************************************************************************
 * READ A BMP INTO A TEXTURE
 *****************************************************************************/
int	ReadInt( FILE * );
short	ReadShort( FILE * );

struct bmfh
{
	short bfType;
	int bfSize;
	short bfReserved1;
	short bfReserved2;
	int bfOffBits;
} FileHeader;

struct bmih
{
	int biSize;
	int biWidth;
	int biHeight;
	short biPlanes;
	short biBitCount;
	int biCompression;
	int biSizeImage;
	int biXPelsPerMeter;
	int biYPelsPerMeter;
	int biClrUsed;
	int biClrImportant;
} InfoHeader;

const int birgb = { 0 };

/**
 ** read a BMP file into a Texture:
 **/

unsigned char *
BmpToTexture( char *filename, int *width, int *height )
{
	
	int s, t, e;		// counters
	int numextra;		// # extra bytes each line in the file is padded with
	FILE *fp;
	unsigned char *texture;
	int nums, numt;
	unsigned char *tp;
	
	
	fp = fopen( filename, "rb" );
	if( fp == NULL )
	{
		fprintf( stderr, "Cannot open Bmp file '%s'\n", filename );
		return NULL;
	}
	
	FileHeader.bfType = ReadShort( fp );
	
	
	// if bfType is not 0x4d42, the file is not a bmp:
	
	if( FileHeader.bfType != 0x4d42 )
	{
		fprintf( stderr, "Wrong type of file: 0x%0x\n", FileHeader.bfType );
		fclose( fp );
		return NULL;
	}
	
	
	FileHeader.bfSize = ReadInt( fp );
	FileHeader.bfReserved1 = ReadShort( fp );
	FileHeader.bfReserved2 = ReadShort( fp );
	FileHeader.bfOffBits = ReadInt( fp );
	
	
	InfoHeader.biSize = ReadInt( fp );
	InfoHeader.biWidth = ReadInt( fp );
	InfoHeader.biHeight = ReadInt( fp );
	
	nums = InfoHeader.biWidth;
	numt = InfoHeader.biHeight;
	
	InfoHeader.biPlanes = ReadShort( fp );
	InfoHeader.biBitCount = ReadShort( fp );
	InfoHeader.biCompression = ReadInt( fp );
	InfoHeader.biSizeImage = ReadInt( fp );
	InfoHeader.biXPelsPerMeter = ReadInt( fp );
	InfoHeader.biYPelsPerMeter = ReadInt( fp );
	InfoHeader.biClrUsed = ReadInt( fp );
	InfoHeader.biClrImportant = ReadInt( fp );
	
	
	// fprintf( stderr, "Image size found: %d x %d\n", ImageWidth, ImageHeight );
	
	
	texture = new unsigned char[ 3 * nums * numt ];
	if( texture == NULL )
	{
		fprintf( stderr, "Cannot allocate the texture array!\b" );
		return NULL;
	}
	
	
	// extra padding bytes:
	
	numextra =  4*(( (3*InfoHeader.biWidth)+3)/4) - 3*InfoHeader.biWidth;
	
	
	// we do not support compression:
	
	if( InfoHeader.biCompression != birgb )
	{
		fprintf( stderr, "Wrong type of image compression: %d\n", InfoHeader.biCompression );
		fclose( fp );
		return NULL;
	}
	
	
	
	rewind( fp );
	fseek( fp, 14+40, SEEK_SET );
	
	if( InfoHeader.biBitCount == 24 )
	{
		for( t = 0, tp = texture; t < numt; t++ )
		{
			for( s = 0; s < nums; s++, tp += 3 )
			{
				*(tp+2) = fgetc( fp );		// b
				*(tp+1) = fgetc( fp );		// g
				*(tp+0) = fgetc( fp );		// r
			}
			
			for( e = 0; e < numextra; e++ )
			{
				fgetc( fp );
			}
		}
	}
	
	fclose( fp );
	
	*width = nums;
	*height = numt;
	return texture;
}

int
ReadInt( FILE *fp )
{
	unsigned char b3, b2, b1, b0;
	b0 = fgetc( fp );
	b1 = fgetc( fp );
	b2 = fgetc( fp );
	b3 = fgetc( fp );
	return ( b3 << 24 )  |  ( b2 << 16 )  |  ( b1 << 8 )  |  b0;
}

short
ReadShort( FILE *fp )
{
	unsigned char b1, b0;
	b0 = fgetc( fp );
	b1 = fgetc( fp );
	return ( b1 << 8 )  |  b0;
}

void
identityMatrix( float matrix[16] ) {
	int i;
	// Set the matrix to identity
	for( i = 0; i < 16; i++ )
		matrix[i] = 0.0;
	for( i = 0; i < 4; i++ )
		matrix[i*5] = 1.0;
}

void
identityQuaternion( float quaternion[4] ) {
	quaternion[0] = 
	quaternion[1] = 
	quaternion[2] = 0.0;
	quaternion[3] = 1.0;
}

void
quat2matrix( float quat[4], float matrix[16] ) {
	float xs, ys, zs, wx, wy, wz, xx, xy, xz, yy, yz, zz;
	
    float t  = 2.0f / ( dot(quat, quat) + quat[3] * quat[3] );
	
    xs = quat[0]*t;		ys = quat[1]*t;		zs = quat[2]*t;
    wx = quat[3]*xs;	wy = quat[3]*ys;	wz = quat[3]*zs;
    xx = quat[0]*xs;	xy = quat[0]*ys;	xz = quat[0]*zs;
    yy = quat[1]*ys;	yz = quat[1]*zs;	zz = quat[2]*zs;
	
    matrix[0 ] = 1.0f-(yy+zz);	matrix[1 ] = xy+wz;			matrix[2 ] = xz-wy;			matrix[3 ] = 0.0f;
	matrix[4 ] = xy-wz;			matrix[5 ] = 1.0f-(xx+zz);	matrix[6 ] = yz+wx;			matrix[7 ] = 0.0f;
	matrix[8 ] = xz+wy;			matrix[9 ] = yz-wx;			matrix[10] = 1.0f-(xx+yy);	matrix[11] = 0.0f;
	matrix[12] = 0.0f;			matrix[13] = 0.0f;			matrix[14] = 0.0f;			matrix[15] = 1.0f;	
}

void
matrixMult( float rotation[16], float matrix[16] ) {
	float accum[16];
#define ROWCOL(i, j) \
	rotation[i*4 + 0] * matrix[ 0 + j] + \
	rotation[i*4 + 1] * matrix[ 4 + j] + \
	rotation[i*4 + 2] * matrix[ 8 + j] + \
	rotation[i*4 + 3] * matrix[12 + j]
	accum[0 ] = ROWCOL(0,0); accum[1 ] = ROWCOL(0,1); accum[2 ] = ROWCOL(0,2); accum[3 ] = ROWCOL(0,3); 
	accum[4 ] = ROWCOL(1,0); accum[5 ] = ROWCOL(1,1); accum[6 ] = ROWCOL(1,2); accum[7 ] = ROWCOL(1,3); 
	accum[8 ] = ROWCOL(2,0); accum[9 ] = ROWCOL(2,1); accum[10] = ROWCOL(2,2); accum[11] = ROWCOL(2,3); 
	accum[12] = ROWCOL(3,0); accum[13] = ROWCOL(3,1); accum[14] = ROWCOL(3,2); accum[15] = ROWCOL(3,3); 
#undef ROWCOL
	
	for( int i = 0; i < 16; i++ ) {
		rotation[i] = accum[i];
	}
}

/*****************************************************************************
*** Arcball rotation class                                                ***
*****************************************************************************/
// [Con|De]structors
ArcBall::ArcBall( GLfloat rotationMatrix[16], int newViewport[4] ) {	
	if( rotationMatrix == NULL ) {
		allocatedMatrix = true;
		matrix = new float[16];
		identityMatrix( matrix );
	} else {
		matrix = rotationMatrix;		
	}
	
	screenResize( newViewport );
}

void
ArcBall::screenResize( int newViewport[4] ) {
	viewport[CENTER_X]	= newViewport[CENTER_X];
	viewport[CENTER_X]	= newViewport[CENTER_Y];
	viewport[WIDTH]		= newViewport[WIDTH];
	viewport[HEIGHT]	= newViewport[HEIGHT];
}

ArcBall::~ArcBall() {
	if( allocatedMatrix )
		delete matrix;
}

// Map values to a sphere
void
ArcBall::map2sphere( float mouseX, float mouseY, float vector[3] ) {
	float temp[3];
	GLfloat magnitude;

	// Map mouse coords to the universal device coords
	float x =  (mouseX / ( (viewport[WIDTH]  - 1.0) / 2.0 )) - 1;
	float y = ((mouseY / ( (viewport[HEIGHT] - 1.0) / 2.0 )) - 1) * -1.0;

	// Load a vector with the device coords and 0 z component
	temp[0] = x;
	temp[1] = y;
	temp[2] = 0.0;
	
	// Find the magnitude of the vector by taking the dot product
    magnitude = dot(temp, temp);

    //If the point is mapped outside of the sphere... (length > radius squared)
    if (magnitude > 1.0f) {
		unit( temp, temp);
		
        //Return the "normalized" vector, a point on the sphere
        vector[0] = temp[0];
        vector[1] = temp[1];
        vector[2] = temp[2];
    } else {
        //Return a vector to a point mapped inside the sphere sqrt(radius squared - length)
        vector[0] = temp[0];
        vector[1] = temp[1];
        vector[2] = sqrt( 1.0f - magnitude );
    }
}

void
// Mouse state modification calls
ArcBall::mouseDown( int x, int y ) {
	// Get the vector of the first start point
	map2sphere( x, y, startVector );
}

void
ArcBall::mouseMotion( int x, int y ) {
	float	axisOfRotation[3];
	float	dragQuaternion[4];
	float	tempRotMatrix[16];
	
	// Get the vector of the second start point
	map2sphere( x, y, endVector );
	
	// Create a quaternion of the drag
	cross( startVector, endVector, axisOfRotation );
	for( int i = 0; i < 3; i++ )
		dragQuaternion[i] = axisOfRotation[i];
	dragQuaternion[3] = dot( startVector, endVector );	
	
	// Make a rotation matrix from the drag quaternion
	quat2matrix( dragQuaternion, tempRotMatrix );
	// Multiply the temp matrix with the rotation matrix
	matrixMult( matrix, tempRotMatrix );
	/*
	//Compute the length of the perpendicular vector if its non-zero
	if( magnitude(axisOfRotation) > Epsilon ) {
		//We're ok, so return the perpendicular vector as the transform after all
		quaternion[0] = axisOfRotation[0];
		quaternion[1] = axisOfRotation[1];
		quaternion[2] = axisOfRotation[2];
		quaternion[3] = dot( startVector, endVector );
	} else { //if its zero, the begin and end vectors coincide, so return an identity transform
		quaternion[0] = 
		quaternion[1] = 
		quaternion[2] = 
		quaternion[3] = 0.0f;
	}
	*/
	startVector[0] = endVector[0];
	startVector[1] = endVector[1];
	startVector[2] = endVector[2];
}

void
ArcBall::mouseUp() {
	return;
}
