/*
 *  Utility.h
 *  VolumeRender
 *
 *  Created by William Dillon on 1/30/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#ifndef UTILITY_H
#define UTILITY_H

#define GLUI_FALSE 0
#define GLUI_TRUE 1

//assuming IEEE-754(GLfloat), which i believe has max precision of 7 bits
# define Epsilon 1.0e-5
#define CENTER_X 0
#define CENTER_Y 1
#define WIDTH	 2
#define HEIGHT   3

/*****************************************************************************
*** Arcball rotation class                                                ***
*****************************************************************************/
class ArcBall {
public:
	// [Con|De]structors
	ArcBall( float rotationMatrix[16], int viewport[4] );
	~ArcBall();
	
	// Mouse state modification calls
	void mouseDown( int x, int y );
	void mouseMotion( int x, int y );
	void mouseUp();
	
	// Reshape
	void screenResize( int viewport[4] );	
private:	
	// Rotation matrix stuff
	bool allocatedMatrix;	// Remember if we have allocated memory
	float *matrix;			// Rotation matrix (may be live variable)
	
	// Sphere-mapped vectors
	float startVector[3], endVector[3];

	// Store the mouse coordinated of the view for sphere map
	void map2sphere( float x, float y, float vector[3] );
	int viewport[4];
};

// Generates a drawing list for a set of axes
int		generateAxesList();
int		generateCubeList();

// Generates a uniform distribution from -1 to 1
float	randomOffset();
void	Arrow( float [3], float [3] );
void	cross( float [3], float [3], float [3] );
float	dot( float [3], float [3] );
float	unit( float [3], float [3] );
float	magnitude( float [3] );

// Generates the square of a float
float	SQR( float value );
float	cube( float value );
float	modelSpaceCoord( float i, float maxDim, float bounds_low, float bounds_high );
int		dataSpaceCoord ( float i, float maxDim, float bounds_low, float bounds_high );

// Functions for converting from scalar space to colorspace
void	scalar2RGB( float scalar, float scalar_high, float scalar_low, float RGB[3] );
void	HSV2RGB(float hsv[3], float rgb[3]);
void	contourQuad( struct xyzs *p0, struct xyzs *p1, struct xyzs *p2, struct xyzs *p3, float scalar );

// Check for OpenGL errors
void	CheckGlErrors( const char* caller );

// Used for the contourQuad function
#define NUMNODES 100
struct	xyzs
{
	float x, y, z;
	float s;
};

// Image file loader
unsigned char *BmpToTexture( char *filename, int *width, int *height );

#endif