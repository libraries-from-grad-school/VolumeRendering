/*
 *  VectorField.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 2/9/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "cmath"
#include "VectorField.h"

// Constants defined from the data
#define SCALAR_LOW  0.0
#define SCALAR_HIGH 2.3

// Options for the probe style radio buttons
#define NONE_PROBE   0
#define POINT_PROBE  1
#define RIBBON_PROBE 2
#define LINE_PROBE   3

// ID for the callback function (largely ignored)
#define DISPLAY 0
#define PROBE 1

// Ugliness to deal with C++ callbacks
VectorField *self;
void GLUIcallback( int id ) { self->callbacks( id ); }

void
initVector( float x, float y, float z, Vector *vector ) {
	vector->direction[0] = y * z * ( y*y + z*z );
	vector->direction[1] = x * z * ( x*x + z*z );
	vector->direction[2] = x * y * ( x*x + y*y );
	vector->magnitude = sqrtf( SQR(vector->direction[0] ) +
							   SQR(vector->direction[1] ) +
							   SQR(vector->direction[2] ) );
}

VectorField::VectorField( int theNumArrows ) {
	// Set the static display lists id's to impossible values
	cubeDisplayList = -1;
	lineSubdiv = 30;
	// Data density
	self = this;
	numArrows = theNumArrows;
}

// GLUI UI init function
void VectorField::initUI(GLUI *gluiIn) {
	glui = gluiIn;
	GLUI_Scrollbar *scroll;
	
	new GLUI_Checkbox( glui, "Draw bounding cube", &drawCube, DISPLAY, GLUIcallback );

	// Vector arrow field
	GLUI_Rollout *arrows = new GLUI_Rollout( glui, "Vector Arrows", true ); {
		new GLUI_Checkbox( arrows, "Draw vector arrows", &drawArrows, DISPLAY, GLUIcallback );
		scroll = new GLUI_Scrollbar( arrows, "Arrow scale", GLUI_SCROLL_HORIZONTAL, &arrowScale, DISPLAY, GLUIcallback );
		scroll->set_float_limits( 0.05, 0.5 );
		new GLUI_StaticText( arrows, "Arrow length" );		
		scroll = new GLUI_Scrollbar( arrows, "Arrow density", GLUI_SCROLL_HORIZONTAL, &numArrows, DISPLAY, GLUIcallback );
		scroll->set_int_limits( 1, 20 );
		new GLUI_StaticText( arrows, "Arrow density" );		
	}

	// Streamlines
	GLUI_Rollout *streamline = new GLUI_Rollout( glui, "Streamlines", true ); {
		new GLUI_Checkbox( streamline, "Show Streamlines", &drawStreamline, DISPLAY, GLUIcallback );
		scroll = new GLUI_Scrollbar( streamline, "Streamline density", GLUI_SCROLL_HORIZONTAL, &numStreamlines, DISPLAY, GLUIcallback );
		scroll->set_int_limits( 1, 20 );
		new GLUI_StaticText( streamline, "Streamline density" );
		scroll = new GLUI_Scrollbar( streamline, "Streamline length", GLUI_SCROLL_HORIZONTAL, &lengthStreamline, DISPLAY, GLUIcallback );
		scroll->set_int_limits( 1, 100 );
		new GLUI_StaticText( streamline, "Streamline length" );	
	}

	GLUI_Rollout *probe = new GLUI_Rollout( glui, "Probe", true ); {
		// Point probe type
		GLUI_RadioGroup *radio = new GLUI_RadioGroup( probe, &probeType, PROBE, GLUIcallback ); {
			new GLUI_RadioButton( radio, "None" );
			new GLUI_RadioButton( radio, "Single Streamline" );
			new GLUI_RadioButton( radio, "Ribbon Trace" );
			new GLUI_RadioButton( radio, "Line trace" );
		}
		// Point probe parameters
		scroll = new GLUI_Scrollbar( probe, "Ribbon width", GLUI_SCROLL_HORIZONTAL, &ribbonWidth, PROBE, GLUIcallback );
		scroll->set_float_limits( 0.01, 0.1 );
		new GLUI_StaticText( probe, "Ribbon width" );
		scroll = new GLUI_Scrollbar( probe, "Elastic line length", GLUI_SCROLL_HORIZONTAL, &lineLength, PROBE, GLUIcallback );
		scroll->set_float_limits( 0.02, 0.25 );
		new GLUI_StaticText( probe, "Elastic line length" );		
		GLUI_Panel *probePanel = new GLUI_Panel( probe, "Probe translation" ); {
			new GLUI_Column( probePanel, false );
			GLUI_Translation *trans = new GLUI_Translation( probePanel, "XY Translation", GLUI_TRANSLATION_XY, &(probeXYZ[0]), PROBE, GLUIcallback );
			trans->set_speed( 0.01 );
			new GLUI_Column( probePanel, false );
			trans = new GLUI_Translation( probePanel, "YZ Translation", GLUI_TRANSLATION_XY, &(probeXYZ[1]), PROBE, GLUIcallback );
			trans->set_speed( 0.01 );		
		}		
	}
	
	displayList = glGenLists( 1 );
	displayListCurrent = false;
	return;
}

void VectorField::draw() {

//	if( displayListCurrent )
//		glCallList( displayList );
//	else
		this->internalDraw();
	
	return;
}

void VectorField::advection( float x, float y, float z, float *newX, float *newY, float *newZ, float *distance ) {
	Vector start, middle, end;
	float xP, yP, zP;
	
	// Get the vector of the starting position
	initVector( x, y, z, &start );
	// Compute the first order steps
	xP = x + start.direction[0] * advectionStep;
	yP = y + start.direction[1] * advectionStep;
	zP = z + start.direction[2] * advectionStep;
	initVector( xP, yP, zP, &middle );
	// Average the vectors
	end.direction[0] = (start.direction[0] + middle.direction[0]) / 2;
	end.direction[1] = (start.direction[1] + middle.direction[1]) / 2;
	end.direction[2] = (start.direction[2] + middle.direction[2]) / 2;
	// Move ahead by the average of the vectors in the passed in variables
	*newX = x + end.direction[0] * advectionStep;
	*newY = y + end.direction[1] * advectionStep;
	*newZ = z + end.direction[2] * advectionStep;
	*distance = sqrtf( SQR(*newX - x) + SQR(*newY - y) + SQR(*newZ - z) );
} 

void VectorField::internalDraw() {
	int i, j, k, l;
	Vector currentVector;
	float tail[3], head[3], RGB[3];
	float ribbon1[4], ribbon2[4], distance;

	if( cubeDisplayList == -1 )
		cubeDisplayList = generateCubeList();
		
	// Store data into display list
//	glNewList( displayList, GL_COMPILE_AND_EXECUTE );

	// Draw the bounding cube
	if( drawCube )
		glCallList( cubeDisplayList );
	
	if( drawArrows == 1 ) {
		for( i = 0; i < numArrows; i += 1 ) {
			for( j = 0; j < numArrows; j += 1 ) {
				for( k = 0; k < numArrows; k += 1 ) {
					// Draw the current vector as an arrow
					initVector( modelSpaceCoord( i + 0.5, numArrows, -1.0, 1.0 ),
								modelSpaceCoord( j + 0.5, numArrows, -1.0, 1.0 ),
								modelSpaceCoord( k + 0.5, numArrows, -1.0, 1.0 ), &currentVector );
					// Set the tail of the arrow to 1/2 the lenght of the arrow behind the point
					tail[0] = modelSpaceCoord( i + 0.5, numArrows, -1.0, 1.0 ) - currentVector.direction[0] * (arrowScale / 2);
					tail[1] = modelSpaceCoord( j + 0.5, numArrows, -1.0, 1.0 ) - currentVector.direction[1] * (arrowScale / 2);
					tail[2] = modelSpaceCoord( k + 0.5, numArrows, -1.0, 1.0 ) - currentVector.direction[2] * (arrowScale / 2);
					// Set the head of the tail as an offset from the tail in the direction of the vector
					head[0] = tail[0] + currentVector.direction[0] * arrowScale;
					head[1] = tail[1] + currentVector.direction[1] * arrowScale;
					head[2] = tail[2] + currentVector.direction[2] * arrowScale;
					// Set the arrow color according to the rainbow scale as a function of the magnitude
					scalar2RGB( currentVector.magnitude, SCALAR_HIGH, SCALAR_LOW, RGB );
					glColor3fv( RGB );
					// Draw the arrow
					Arrow( tail, head );
				}							
			}
		}
	}
	if( drawStreamline ) {
		glBegin( GL_LINES );
		for( i = 0; i < numStreamlines; i++ ) {
			for( j = 0; j < numStreamlines; j++ ) {
				for( k = 0; k < numStreamlines; k++ ) {
					// Draw a streamline from the start position
					tail[0] = modelSpaceCoord( i + 0.5, numStreamlines, -1.0, 1.0 );
					tail[1] = modelSpaceCoord( j + 0.5, numStreamlines, -1.0, 1.0 );
					tail[2] = modelSpaceCoord( k + 0.5, numStreamlines, -1.0, 1.0 );
					for( l = 0; l < lengthStreamline &&
								tail[0] > -1.0 && tail[0] < 1.0 &&
								tail[1] > -1.0 && tail[1] < 1.0 &&
								tail[2] > -1.0 && tail[2] < 1.0; l++ ) {
						advection( tail[0], tail[1], tail[2], &head[0], &head[1], &head[2], &distance);
						scalar2RGB( distance, SCALAR_HIGH * advectionStep, SCALAR_LOW, RGB );
						glColor3fv( RGB );
						glVertex3fv( tail );
						glVertex3fv( head );
						tail[0] = head[0];
						tail[1] = head[1];
						tail[2] = head[2];
					}
				}
			}
		}
		glEnd();
	}
	if( probeType > NONE_PROBE ) {
		
		// Draw a cube at the probe point
		glPushMatrix();
		glTranslatef( probeXYZ[0], probeXYZ[1], probeXYZ[2] );
		glScalef( 0.01, 0.01, 0.01 );
		glCallList( cubeDisplayList );
		glPopMatrix();
		
		// Draw the probe as currently selected
		switch( probeType ) {
			case POINT_PROBE:
				glBegin( GL_LINES );
				// Draw a streamline initial position
				tail[0] = probeXYZ[0];
				tail[1] = probeXYZ[1];
				tail[2] = probeXYZ[2];
				// Draw the streamline
				i = 0;
				do {
					advection( tail[0], tail[1], tail[2], &head[0], &head[1], &head[2], &distance);
					scalar2RGB( distance, SCALAR_HIGH * advectionStep, SCALAR_LOW, RGB );
					glColor3fv( RGB );
					glVertex3fv( tail );
					glVertex3fv( head );
					tail[0] = head[0];
					tail[1] = head[1];
					tail[2] = head[2];
					i++;
				} while( tail[0] > -1.0 && tail[0] < 1.0 &&
						 tail[1] > -1.0 && tail[1] < 1.0 &&
						 tail[2] > -1.0 && tail[2] < 1.0 && i < 100000 );
				glEnd();
				break;
			case RIBBON_PROBE:
				glBegin( GL_QUAD_STRIP );
				ribbon1[0] = ribbon2[0] = probeXYZ[0];
				ribbon1[1] = probeXYZ[1] + ribbonWidth / 2.0;
				ribbon2[1] = probeXYZ[1] - ribbonWidth / 2.0;
				ribbon1[2] = ribbon2[2] = probeXYZ[2];
				glColor3f( 1.0, 1.0, 1.0 );
				glVertex3fv( ribbon1 );
				glVertex3fv( ribbon2 );
				// Draw the streamline
				i = 0;
				do {
					advection(  ribbon1[0],  ribbon1[1],  ribbon1[2],
							   &ribbon1[0], &ribbon1[1], &ribbon1[2], &ribbon1[3] );
					scalar2RGB( ribbon1[3], SCALAR_HIGH * advectionStep, SCALAR_LOW, RGB );
					glColor3fv( RGB );
					glVertex3fv( ribbon1 );
					advection(  ribbon2[0],  ribbon2[1],  ribbon2[2],
							   &ribbon2[0], &ribbon2[1], &ribbon2[2], &ribbon2[3] );
					scalar2RGB( ribbon2[3], SCALAR_HIGH * advectionStep, SCALAR_LOW, RGB );
					glColor3fv( RGB );
					glVertex3fv( ribbon2 );
					i++;
				} while( ribbon1[0] > -1.0 && ribbon1[0] < 1.0 &&
						 ribbon1[1] > -1.0 && ribbon1[1] < 1.0 &&
						 ribbon1[2] > -1.0 && ribbon1[2] < 1.0 &&
						 ribbon2[0] > -1.0 && ribbon2[0] < 1.0 &&
						 ribbon2[1] > -1.0 && ribbon2[1] < 1.0 &&
						 ribbon2[2] > -1.0 && ribbon2[2] < 1.0 && i < 10000 );
				glEnd();
				break;
			case LINE_PROBE:
				glColor3f( 1.0, 1.0, 1.0 );
				glBegin( GL_LINE_STRIP );
				glVertex3fv( start );
				for( i = 0; i < lineSubdiv; i++ )
					glVertex3fv( linePoints[i] );
				glVertex3fv( end );
				glEnd();
				break;
		}
	}
	
//	glEndList();
//	displayListCurrent = true;
	
	return;
}

void VectorField::reinterpLine() {
	for( int i = 0; i < lineSubdiv; i++ ) {
		linePoints[i][0] = start[0] + ( (end[0] - start[0]) * ((float)i / (float)lineSubdiv));
		linePoints[i][1] = start[1] + ( (end[1] - start[1]) * ((float)i / (float)lineSubdiv));
		linePoints[i][2] = start[2] + ( (end[2] - start[2]) * ((float)i / (float)lineSubdiv));
	}	
}
void VectorField::callbacks( int id ) {
	
	displayListCurrent = false;
	
	if( id == PROBE ) {
		// Set the advection start and end points
		start[0] = probeXYZ[0] + lineLength * 0.5;
		start[1] = probeXYZ[1] + lineLength * 0.5;
		start[2] = probeXYZ[2] + lineLength * 0.5;
		  end[0] = probeXYZ[0] - lineLength * 0.5;
		  end[1] = probeXYZ[1] - lineLength * 0.5;
		  end[2] = probeXYZ[2] - lineLength * 0.5;
		reinterpLine();		
	}
	  
	glui->sync_live();

		
}

void VectorField::animate() {
	int i, j;
	// Compute cycle time
	float newTime, temp;
	int timeMS = glutGet( GLUT_ELAPSED_TIME );
	newTime = float( timeMS % (int)(secPerCycle * 1000)) / (secPerCycle * 1000);

	if( probeType == LINE_PROBE ) {
		temp = (newTime - time) * 10000.0;
		for( i = 0; i < (int)temp; i++ ) {
			for( j = 0; j < lineSubdiv; j++ ) {
				if( linePoints[j][0] < 1.0 && linePoints[j][0] > -1.0 &&
					linePoints[j][1] < 1.0 && linePoints[j][1] > -1.0 &&
					linePoints[j][2] < 1.0 && linePoints[j][2] > -1.0 ) {
					advection(  linePoints[j][0],  linePoints[j][1],  linePoints[j][2],
								&linePoints[j][0], &linePoints[j][1], &linePoints[j][2], &temp );
				} else {
					reinterpLine();					
				}				
			}
		}
		glutPostRedisplay();
	}
	
	time = newTime;
}

void VectorField::reset() {
	displayListCurrent = false;
	
	// Arrow parameters
	drawArrows = 0;
	numArrows = 7;
	arrowScale = 0.25;

	// Streamline parameters
	drawStreamline = 1;
	advectionStep = 0.01;
	numStreamlines = 5;
	lengthStreamline = 25;
	
	// Probe parameters
	ribbonWidth = 0.5;
	probeType = NONE_PROBE;
	probeXYZ[0] = probeXYZ[1] = probeXYZ[2] = 0.0;
	// Set the advection start and end points
	lineLength = 0.2;
	start[0] = probeXYZ[0] + 0.25;
	start[1] = probeXYZ[1] + 0.25;
	start[2] = probeXYZ[2] + 0.25;
	  end[0] = probeXYZ[0] - 0.25;
	  end[1] = probeXYZ[1] - 0.25;
	  end[2] = probeXYZ[2] - 0.25;	
	secPerCycle = 3.0;	
	reinterpLine();
	lastMS = 0;
	time = 0.0;
	
	glui->sync_live();
	
	return;
}