/*
 *  VectorField.h
 *  VolumeRender
 *
 *  Created by William Dillon on 2/9/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include "Utility.h"
#include "Model.h"

#ifndef VECTORFIELD_H
#define VECTORFIELD_H

#ifdef VECTORFIELD
#define ANIMATE
#endif

typedef struct {
	float direction[3];
	float magnitude;
} Vector;

class VectorField : public Model {
public:
	// Constructor
	VectorField( int size );
	~VectorField();
	
	// UI Creation and OpenGL init
	void initUI( GLUI *glui );
	
	// Callbacks
	void draw();
	void reset();
	void animate();
	void callbacks( int id );

private:
	
	// Display list for the whole scene (for transformation)
	int displayList;
	bool displayListCurrent;
	// Display list for the bounding cube (static)
	int drawCube;
	int cubeDisplayList;	
	// Vector arrow options
	float arrowScale;
	int numArrows;
	int drawArrows;
	// Line advection
	int drawStreamline;
	int numStreamlines;
	int lengthStreamline;
	float advectionStep;
	void advection( float x, float y, float z, float *newX, float *newY, float *newZ, float *distance );
	// Probe trace
	int probeType;
	float probeXYZ[3];
	float ribbonWidth;
	
	// Animation parameters
	int lastMS;
	float time;
	float secPerCycle;
	// Line trace
	int lineSubdiv;
	float lineLength;
	float linePoints[30][3];
	float start[3], end[3];
	void reinterpLine();
	
	void internalDraw( void );
	
	// References to GLUI elements
	GLUI *glui;
};

#endif