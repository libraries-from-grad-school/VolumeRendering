/*
 *  VolumeData.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 2/3/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <cmath>
#include "VolumeData.h"
#include "Utility.h"
#include <glui.h>
#include <GLUT/glut.h>

// Hand-edited transfer function
#define XFERFUNCTEXSIZE 8
const float xferFunc[8][4] = {
	{ 0.0, 0.0, 0.0, 0.0   },	// Transparent black
	{ 1.0, 0.0, 0.0, 0.010 },	// Red
	{ 0.7, 0.2, 0.0, 0.052 },	// Orange
	{ 0.9, 0.7, 0.0, 0.2   },	// Yellow
	{ 1.0, 1.0, 1.0, 1.0   },   // White
	{ 1.0, 1.0, 1.0, 1.0   },   // White
	{ 1.0, 1.0, 1.0, 1.0   },   // White
	{ 1.0, 1.0, 1.0, 1.0   }    // White
};

// 3x3 blending function
const float threeByBlend[ 3*3 ] = { 0.25, 0.5, 0.25,
									0.5,  1.0, 0.5,
									0.25, 0.5, 0.25 };

// Ugliness to deal with C++ and callbacks
static VolumeData *thisObject;


// Constructor and destructor
#pragma mark [Con|De]structor
VolumeData::VolumeData( char *filename, int dataWidth, int dataHeight, int dataDepth, int dataBitDepth ) {
	
	thisObject = this;
	// Make a local copy of the dimensions of the data
	width = dataWidth;
	height = dataHeight;
	depth = dataDepth;
	maxScalar = (int)pow( float(2), float(dataBitDepth) ); // 2^bitDepth to get the maximum scalar
	
	// Allocate data for pre-computed dataset and compute the data points
	data = new float[ width * height * depth];
	
	// Load the dataset from file
	if( loadFile( filename, width, height, depth, dataBitDepth) )
		return;

	printf( "Unable to load dataset.\n" );
	delete this->data;
}

VolumeData::~VolumeData() {
	delete this->data;
}

// Dataset Loading and managment
#pragma mark -
#pragma mark Dataset managment
#define BLOCK_SIZE	512
#define SCALAR_HIGH maxScalar
#define SCALAR_LOW	  0.0

// Load the data from a file (currently only support double byte little endian)
int VolumeData::loadFile( char *filename, int fileWidth, int fileHeight, int fileDepth, int fileBitDepth ) {
	unsigned int i, readBytes = 0, count = 0;
	unsigned char inBuffer[BLOCK_SIZE];
	unsigned int temp;

	FILE *fileID;
	
	fileID = fopen( filename, "rb" ); // Open the file for binary reading
	if( fileID == NULL ) {
		printf("Error opening file: %s\n", filename);
		return 0;
	}
	
	do {
		readBytes = fread( (void *)inBuffer, 1, BLOCK_SIZE, fileID );
		if( readBytes >= 2 ) {
			for( i = 0; i < readBytes / 2; i++ ) {
				temp = inBuffer[i * 2 + 1];	// Copy the high byte
				temp <<= 8;					// Shift the high byte to its home
				temp |= inBuffer[i * 2];	// Boolean OR the low byte temp should be the value now

				// Normallize the scalar and store it
				data[count++] = float( temp ) / 3500.0;
			}
		} else if( readBytes == 1 )
			printf( "File byte alignment error (trailing byte)\n" );
	} while( readBytes == BLOCK_SIZE );
	
	fclose( fileID );
	return 1;
}

float VolumeData::getData( int x, int y, int z ) {
	return data[ x + y * (width) + z * (width * height) ];
}

// Trilinearly interpolate the dataset to the given point
float VolumeData::getData( float x, float y, float z ) {
//	float	retval = 0.0;
	float	s1, s2, s3, s4, s5, s6, s7, s8;
	int		x1, x2, y1, y2, z1, z2;

	// Clamp input values to the bounds of the dataset (no extrapolation)
	if( x < 0.0 )
		x = 0.0;
	if( y < 0.0 )
		y = 0.0;
	if( z < 0.0 )
		z = 0.0;
	if( x > float(width) )
		x = float(width);
	if( y > float(height) )
		y = float(height);
	if( z > float(depth) )
		z = float(depth);
	
	// Find the bounding cube planes
	x1 = int( floor(x) );
	y1 = int( floor(y) );
	z1 = int( floor(z) );
	x2 = int( ceil(x)  );
	y2 = int( ceil(y)  );
	z2 = int( ceil(z)  );
	
	// Find the scalar values of the verticies of the cube
	s1 = getData( x1, y1, z1 );
	s2 = getData( x1, y1, z2 );
	s3 = getData( x1, y2, z1 );
	s4 = getData( x1, y2, z2 );
	s5 = getData( x2, y1, z1 );
	s6 = getData( x2, y1, z2 );
	s7 = getData( x2, y2, z1 );
	s8 = getData( x2, y2, z2 );

	// Begin accumulating the terms of the interpolation
//	retval = 
	return getData( x1, y1, z1 );
}

#pragma mark -
#pragma mark UI and Drawing routines

// ID's for the range sliders and their callback
#define XRANGE 1
#define YRANGE 2
#define ZRANGE 3
#define TRANGE 4

// Callback that can be used by GLUI
void Sliders( int id ) { thisObject->sliderCallback( id ); }
void Checkboxes( int id) { thisObject->checkboxCallback( id ); }

// Create the GLUI widgets necessary for this model
void VolumeData::initUI( GLUI *glui ) {
//	GLUI_RangeSlider *range;
	// Parameters for point cloud
//	GLUI_Rollout *pointsPanel = new GLUI_Rollout( glui, "Points" ); {
		// Display the data as points?
//		new GLUI_Checkbox( pointsPanel, "Splats", &splats, 0, Checkboxes );
		// Options for points display
//		new GLUI_Checkbox( pointsPanel, "Billboard", &billboard, 1, Checkboxes );
//	}
	
	volumeDisplayList = glGenLists( 1 );
}

// Callbacks
void VolumeData::draw() {
	if( displayListCurrent )
		glCallList( volumeDisplayList );
	else
		internalDraw();
	
}

#define X_AXIS 0
#define Y_AXIS 1
#define Z_AXIS 2

void VolumeData::internalDraw() {
	GLdouble modelViewMatrix[16], projectionMatrix[16];
	GLint viewport[4];
	float scalar;
	float x, y, z;
	GLdouble x1, y1, z1, x2, y2, z2;
	int i, j, k, axis, direction;
	float majorDimension, scaleFactor; // Largest dimension of the model
	
	if( width >= height && width >= depth )
		majorDimension = width;
	if( height >= width && height >= depth )
		majorDimension = height;
	if( depth >= width && depth >= height )
		majorDimension = depth;
	
	scaleFactor = 2.0 / majorDimension;
	
	glPointSize( 2.0 );
	
	// Store data into display list
	glNewList( volumeDisplayList, GL_COMPILE_AND_EXECUTE );

	// Load the transfer function into a texture
	glTexImage1D( GL_TEXTURE_1D, 0, GL_RGBA, XFERFUNCTEXSIZE, 0, GL_RGBA, GL_FLOAT, xferFunc );
	glTexParameterf( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_1D, GL_REPLACE );
	glEnable( GL_TEXTURE_1D );
	
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
//	glEnable( GL_BLEND );
	
	// Save the entering modelview matrix
	glPushMatrix();
	// Scale and translate the modelview to allow further calls to be in model space
	glScalef( scaleFactor, scaleFactor, scaleFactor );
	glTranslated( width / -2, height / -2, depth / -2 );
	glPointSize( 1.0 );
	
	// Project a ray into the volume
	glGetIntegerv( GL_VIEWPORT, viewport );
	glGetDoublev(  GL_MODELVIEW_MATRIX,  modelViewMatrix );
	glGetDoublev( GL_PROJECTION_MATRIX, projectionMatrix );
	gluUnProject( 0.5, 0.5, 0.0, modelViewMatrix, projectionMatrix, viewport, &x1, &y1, &z1 );
	gluUnProject( 0.5, 0.5, 1.0, modelViewMatrix, projectionMatrix, viewport, &x2, &y2, &z2 );
	// Find the vector from the back of the clipping volume to the front
	x1 -= x2;
	y1 -= y2;
	z1 -= z2;
	// Find the axis most parallel with the view and in which orientation
	if( abs(x1) > abs(y1) && abs(x1) > abs(z1) ) {
		axis = X_AXIS;
		direction = x1 / (float)abs(x1);
	} else if( abs(y1) > abs(z1) ) {
		axis = Y_AXIS;
		direction = y1 / (float)abs(y1);
	} else {
		axis = Z_AXIS;		
		direction = z1 / (float)abs(z1);
	}
	
	glBegin( GL_POINTS );
//	if( 
	
	for( i = zRange[0]; i < floor(zRange[1]); i += 2 ) {			
		for( j = yRange[0]; j < floor(yRange[1]); j += 2 ) {
			for( k = xRange[0]; k < floor(xRange[1]); k += 2 ) {
				scalar = getData(k, j, i);
				x = k;
				y = j;
				z = i;
				
				// Place the point
				glTexCoord1f( scalar );
				glVertex3f( x, y, z );
			}
		}
	}
	glEnd();
	
	glDisable( GL_TEXTURE_1D );
//	glDisable( GL_BLEND );
	glPopMatrix();
	
	glEndList();
	displayListCurrent = true;
}

void VolumeData::reset() {
	// Set the range sliders appropriately
	xRange[0] = yRange[0] = zRange[0] = 0;
	xRange[1] = width;
	yRange[1] = height;
	zRange[1] = depth;
	sRange[0] = 0.0;
	sRange[1] = 1.0;
	
	// Set the boolean options to defaults
	points = 1;
	jitter = 0;
	
	// Re-seed the random number generator
	srandom( time(NULL) );

	displayListCurrent = false;

}

void VolumeData::checkboxCallback( int id ) {
	displayListCurrent = false;
}

void VolumeData::sliderCallback( int id ) {
	char str[32];
	
	displayListCurrent = false;
		
	switch( id ) {
		case XRANGE:
			sprintf( str, "X Range: %f to %f", xRange[0], xRange[1] );
			xRangeLabel->set_text( str );
			break;
		case YRANGE:
			sprintf( str, "Y Range: %f to %f", yRange[0], yRange[1] );
			yRangeLabel->set_text( str );
			break;
		case ZRANGE:;
			sprintf( str, "Z Range: %f to %f", zRange[0], zRange[1] );
			zRangeLabel->set_text( str );
			break;
			//		case RRANGE:
			//			sprintf( str, "Radial Dist.: %f to %f", rRange[0], rRange[1] );
			//			rRangeLabel->set_text( str );
			//			break;
		case TRANGE:
			sprintf( str, "Scalar Range: %f to %f", sRange[0], sRange[1] );
			sRangeLabel->set_text( str );
			break;
	}
}
