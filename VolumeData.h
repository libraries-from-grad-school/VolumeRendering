/*
 *  VolumeData.h
 *  VolumeRender
 *
 *  Created by William Dillon on 2/3/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include "Model.h"
#include "PointField.h"

#ifndef VOLUMEDATA_H
#define VOLUMEDATA_H

class VolumeData : public Model {
public:
	// Constructor
	VolumeData( char *filename, int dataWidth, int dataHeight, int dataDepth, int dataBitDepth );
	~VolumeData();
	
	// UI Creation
	void initUI( GLUI *glui );
	
	// Callbacks
	void sliderCallback( int id );
	void checkboxCallback( int id );
	void draw();
	void reset();
private:
	int loadFile( char *filename, int dataWidth, int dataHeight, int dataDepth, int dataBitDepth );
	void  setData( int u, int v, int w, float s );
	float getData( int u, int v, int w );
	// Note that this form of getData does not used cached values
	float getData( float u, float v, float w );
	
	// State variables that dictate drawing behavior
	int points, jitter;
	float xRange[2], yRange[2], zRange[2], tRange[2];
	float sRange[2];
	// State variables pertaining to and the data
	int width, height, depth;
	int maxScalar;
	float *data;
	float *transferTexture;
	
	// Display list stuff
	int volumeDisplayList;
	bool displayListCurrent;
	void internalDraw( void );
		
	// References to GLUI elements
	GLUI *glui;
	GLUI_StaticText *xRangeLabel;
	GLUI_StaticText *yRangeLabel;
	GLUI_StaticText *zRangeLabel;
	GLUI_StaticText *sRangeLabel;
};
#endif