/*
 *  VolumeRender.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 2/27/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "Utility.h"
#include <cmath>
#include <glui.h>
#include <GLUT/glut.h>
#include "VolumeRender.h"
#include "VolumeUI.h"

// Ugliness to deal with C++ and callbacks
static VolumeRender *thisObject;

// Constructor and destructor
#pragma mark [Con|De]structor
VolumeRender::VolumeRender( int newWidth, int newHeight, int newDepth ) {
	// Make a local copy of the dimensions of the data
	width = newWidth;
	height = newHeight;
	depth = newDepth;
	
	// Save a static copy of the self-referential pointer
	thisObject = this;
	this->reset();

	// Allocate data for pre-computed dataset and compute the data points
	data = new float[ width * height * depth * 4 ];
	
	// Compute pre-cached data for each point
	this->computeData();	
}

VolumeRender::VolumeRender( char *filename ) {
	// Loading volume dataset into RAM
	FILE *fileID;
	
	fileID = fopen( filename, "rb" ); // Open the file for binary reading
	if( fileID == NULL ) {
		fprintf(stderr, "Error opening file: %s\n", filename);
		exit(EXIT_FAILURE);
	}
	
	fread( (void *)&width,  sizeof(int), 1, fileID );
	fread( (void *)&height, sizeof(int), 1, fileID );
	fread( (void *)&depth,  sizeof(int), 1, fileID );
	
	// Save a static copy of the self-referential pointer
	thisObject = this;
	this->reset();

	int texture_size = width * height * depth * 4 * sizeof(float);
	data = (float *)malloc(texture_size);

	fread( (void *)data, texture_size, 1, fileID);

	fclose( fileID );

	printf("Opened %s: (%d,%d,%d).\n", filename, width, height, depth);
	
}

VolumeRender::~VolumeRender() {
	// Deallocate the storage for the dataset
	delete this->data;
}

// Dataset Creation and managment
#pragma mark -
#pragma mark Dataset managment
// Bounds of the dataset in model space
#define BOUNDS_LOW	 -1.0
#define BOUNDS_HIGH   1.0
#define SCALAR_HIGH 100.0
#define SCALAR_LOW	  0.0

struct centers {
	float xc, yc, zc;       // center location
	float a;                // amplitude
} Centers[] = {
	{ 1.00f, 0.00f, 0.00f,  90.00f },
	{-1.00f, 0.30f, 0.00f, 120.00f },
	{ 0.00f, 1.00f,	0.00f, 120.00f },
	{ 0.00f, 0.40f,	1.00f, 170.00f } };

inline float SQR( float in ) { return in * in; }

float
SValue( float x, float y, float z )
{
	int i;                  // counter
	float s;                // scalar value
	float dx, dy, dz, dsqd; // displacements
	
	s = 0.0;
	
	for( i=0; i < 4; i++ )
	{
		dx = x - Centers[i].xc;
		dy = y - Centers[i].yc;
		dz = z - Centers[i].zc;
		dsqd = SQR(dx) + SQR(dy) + SQR(dz);
		s += Centers[i].a * exp( -5.*dsqd );
	}
	
	return s;
}

void VolumeRender::setData( int x, int y, int z, float scalar ) {
	float RGB[3];
	scalar2RGB( scalar, SCALAR_HIGH, SCALAR_LOW, RGB );
		
	if( scalar > tRange[0] && scalar < tRange[1] ) {
		data[0 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = RGB[0];
		data[1 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = RGB[1];
		data[2 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = RGB[2];
		data[3 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = modelSpaceCoord( scalar, tRange[1], aRange[0], aRange[1] );		
	} else {
		data[3 + (x * 4) + (y * width * 4) + (z * width * height * 4)] = 0.0;				
	}
}

void VolumeRender::computeData() {
	int i, j, k;
	
	// Compute all the points
	for( i = 0; i < depth; i++ )
		for( j = 0; j < height; j++ )
			for( k = 0; k < width; k++ )
				setData( k, j, i, SValue( modelSpaceCoord( k, width,  BOUNDS_LOW, BOUNDS_HIGH ),
										  modelSpaceCoord( j, height, BOUNDS_LOW, BOUNDS_HIGH ),
										  modelSpaceCoord( i, depth,  BOUNDS_LOW, BOUNDS_HIGH ) ) );
	
	// Clear the alpha bits of the edges
	for( i = 0; i < depth; i++ )
		for( j = 0; j < height; j++ ) {
			setData (    0   , j, i, -1.0 );
			setData ( width-1, j, i, -1.0 );
		}
	for( j = 0; j < height; j++ )
		for( k = 0; k < width; k++ ) {
			setData ( k, j,    0   , -1.0 );
			setData ( k, j, depth-1, -1.0 );
		}
	for( k = 0; k < width; k++ )
		for( i = 0; i < depth; i++ ) {
			setData( k,     0   , i, -1.0 );
			setData( k, height-1, i, -1.0 );
		}
}

// UI and Drawing routines
#pragma mark -
#pragma mark UI and Drawing routines
// ID's for GLUI callbacks
#define MODEL 0
#define TEXTURE 1

// Callback that can be used by GLUI
void Sliders( int id ) { thisObject->sliderCallback( id ); }
void Checkboxes( int id) { thisObject->checkboxCallback( id ); }

// Create the GLUI widgets necessary for this model
void VolumeRender::initUI( GLUI *glui ) {
	GLUI_Scrollbar *scroll;
//	GLUI_RangeSlider *range;
	
	// Parameters for volume render
	GLUI_Panel *pointsPanel = new GLUI_Panel( glui, "", false ); {
		// T filter values
//		range = new GLUI_RangeSlider( pointsPanel, "Temp Values", tRange, TEXTURE, Sliders );
//		range->set_float_limits( SCALAR_LOW, SCALAR_HIGH );
		new GLUI_StaticText( pointsPanel, "T Points Range" );
		// A filter values
//		range = new GLUI_RangeSlider( pointsPanel, "Alpha Values", aRange, TEXTURE, Sliders );
//		range->set_float_limits( 0.0, 1.0 );
		new GLUI_StaticText( pointsPanel, "Alpha range Points Range" );
		// Number of slices
		scroll = new GLUI_Scrollbar( pointsPanel, "Total slices", GLUI_SCROLL_HORIZONTAL, &numSlices, MODEL, Sliders );
		scroll->set_int_limits( 30, 300 );
		new GLUI_StaticText( pointsPanel, "Number of slices" );
		// First slab slice multiplier
		scroll = new GLUI_Scrollbar( pointsPanel, "first slab slices", GLUI_SCROLL_HORIZONTAL, &firstMultiplier, MODEL, Sliders );
		scroll->set_float_limits( 1.0, 10.0 );
		new GLUI_StaticText( pointsPanel, "First slab bias" );
		// Second slab slice multiplier
		scroll = new GLUI_Scrollbar( pointsPanel, "second slab slices", GLUI_SCROLL_HORIZONTAL, &firstMultiplier, MODEL, Sliders );
		scroll->set_int_limits( 1.0, 10.0 );
		new GLUI_StaticText( pointsPanel, "Second slab bias" );		
	}
		
	// Get OpenGL retained mode graphics ID's
	volumeDisplayList = glGenLists( 1 );// Request display list ID from OpenGL
	glGenTextures(1, &texID);			// Request texture ID from OpenGL
			
}

// Callbacks
void VolumeRender::draw() {
	int i, j;
	float transverseMatrix[16];
	
	for( i = 0; i < 4; i++ )
		for( j = 0; j < 4; j++ )
			transverseMatrix[ i + j*4 ] = theUI->rotateMatrix[ i*4 + j ];
	
	if( !textureCurrent ) {
//		this->computeData();
		this->refreshTexture();
	}
	
	// Save the previous matrix on the stack and load identity
	glPushMatrix();
	glLoadIdentity();
	gluLookAt( 0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	// Set the matrix mode to modify the texture coords
	glMatrixMode( GL_TEXTURE );
	glScalef( 1.0 / (GLfloat)theUI->scale,
			  1.0 / (GLfloat)theUI->scale,
			  1.0 / (GLfloat)theUI->scale );
//	fprintf( stderr, "Incoming scale factor %f, new scale factor %f.\n", theUI->scale, 1.0 / theUI->scale );
	glTranslatef( (GLfloat)theUI->transXYZ[0] * -1.0 + 0.5,
				  (GLfloat)theUI->transXYZ[1] * -1.0 + 0.5,
				  (GLfloat)theUI->transXYZ[2] * -1.0 + 0.5 );
	glMultMatrixf( (const GLfloat *)transverseMatrix );
//	glRotatef( theUI->rotateX * -1.0, 1.0, 0.0, 0.0 );
//	glRotatef( theUI->rotateY * -1.0, 0.0, 1.0, 0.0 );
	
	if( displayListCurrent ) {
		glCallList( volumeDisplayList );
	} else {
		this->internalDraw();
	}
		
	// Clean-up the state
	glLoadIdentity();
	glMatrixMode( GL_MODELVIEW_MATRIX );
	glPopMatrix();
	glDisable( GL_TEXTURE_3D );
		
	return;
}

//#define SLICES 150
void VolumeRender::internalDraw() {
	int i;
	float zVal = -2.0;
//	float matrix[16];
		
	glNewList( volumeDisplayList, GL_COMPILE_AND_EXECUTE );

	// Set texture parameters
	glEnable( GL_TEXTURE_3D );
	glBindTexture( GL_TEXTURE_3D, texID );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	// Draw the view-aligned planes
	glBegin( GL_QUADS ); {
		for( i = 0; i < numSlices; i++ ) {
//			fprintf( stderr, "Zval: %f, rate: %f\n", zVal, 4.0/(float)numSlices * (1.0/(float)firstMultiplier) );
			glTexCoord3f(  2.0,  2.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			glVertex3f  (  3.0,  3.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			glTexCoord3f(  2.0, -2.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			glVertex3f  (  3.0, -3.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			glTexCoord3f( -2.0, -2.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			glVertex3f  ( -3.0, -3.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			glTexCoord3f( -2.0,  2.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			glVertex3f  ( -3.0,  3.0, zVal ); // modelSpaceCoord( i, SLICES, -2.0, 2.0 ) );
			if( zVal < -1.0 ) {
				zVal += (4.0/(float)numSlices) * (float)firstMultiplier;
			} else if( zVal < 0.0 ) {
				zVal += (4.0/(float)numSlices) * (float)secondMultiplier;
			} else if( zVal < 1.0 ) {
				zVal += (4.0/(float)numSlices) * (1.0/(float)secondMultiplier);
			} else
				zVal += (4.0/(float)numSlices) * (1.0/(float)firstMultiplier);
		}
	} glEnd();
	
	glEndList();
	displayListCurrent = true;
}

// Reload the texture to the GPU
void VolumeRender::refreshTexture( void ) {
	
	glBindTexture( GL_TEXTURE_3D, texID );
	
//	glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
	
//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_STORAGE_HINT_APPLE, GL_STORAGE_SHARED_APPLE);
//	glTextureRangeAPPLE(GL_TEXTURE_3D, width * height * depth * 4 * sizeof(float), data);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_RGBA, GL_FLOAT, data );
	
	textureCurrent = true;
}

void VolumeRender::reset() {
	// Set the range sliders appropriately
	aRange[0] = 0.0;
	aRange[1] = 1.0;
	tRange[0] = SCALAR_LOW;
	tRange[1] = SCALAR_HIGH;
	
	// Parameters for the number of slices
	numSlices = 75;
	firstMultiplier = 4.0;
	secondMultiplier = 2.0;
	
	// Make sure that everything is redrawn
	displayListCurrent = false;
	textureCurrent = false;
}

void VolumeRender::checkboxCallback( int id ) {
	displayListCurrent = false;
		textureCurrent = false;
}

void VolumeRender::sliderCallback( int id ) {	
	switch( id ) {
		case TEXTURE:
			textureCurrent = false;
			break;
		case MODEL:
			displayListCurrent = false;
			break;
	}
}

