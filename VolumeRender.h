/*
 *  VolumeRender.h
 *  VolumeRender
 *
 *  Created by William Dillon on 2/27/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include "Utility.h"
#include "Model.h"

#ifndef VOLUME_RENDER_H
#define VOLUME_RENDER_H

class VolumeRender : public Model {
public:
	// Constructor
	VolumeRender( int newWidth, int newHeight, int newDepth );
	VolumeRender( char *filename );
	~VolumeRender();
	
	// UI Creation
	void initUI( GLUI *glui );
	
	// Callbacks
	void sliderCallback( int id );
	void checkboxCallback( int id );
	void draw();
	void reset();
	
private:
	// Functions to create and manage the dataset
	void computeData();

	// Variable for data filtering (will be uniform variables)
	float aRange[2], tRange[2];
	
	// State variables pertaining to and the data
	void setData( int u, int v, int w, float s );
	int width, height, depth;
	float *data;
	
	// State variables pertaining to the texture
	GLuint texID;
	bool textureCurrent;
	void refreshTexture( void );

	// Use display lists whenever possible
	int volumeDisplayList;
	bool displayListCurrent;
	void internalDraw( void );
	// Variables for slice density
	int	numSlices;
	float firstMultiplier;
	float secondMultiplier;
	
	// References to GLUI elements
	GLUI *glui;
	GLUI_StaticText *xRangeLabel;
	GLUI_StaticText *yRangeLabel;
	GLUI_StaticText *zRangeLabel;
	GLUI_StaticText *tRangeLabel;	
};
#endif
