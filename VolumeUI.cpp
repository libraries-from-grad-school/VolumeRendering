/*
 *  VolumeUI.cpp
 *  VolumeRender
 *
 *  Created by William Dillon on 1/30/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include "VolumeUI.h"
#include "Model.h"
#include "Utility.h"
#include "math.h"
#include "iostream"
//#include </Library/Frameworks/GLUI.framework/Headers/arcball.h>

using namespace std;

#pragma mark Constants and Globals
// Initial window parameters
#define WINDOW_INIT_SIZE_X 600
#define WINDOW_INIT_SIZE_Y 600
#define WINDOW_INIT_X 60
#define WINDOW_INIT_Y 60

// Bit values for mouse buttons
const int LEFT   = { 4 };
const int MIDDLE = { 2 };
const int RIGHT  = { 1 };

// fog parameters:
const GLfloat FOGCOLOR[4] = { .0, .0, .0, 1. };
const GLenum  FOGMODE     = { GL_LINEAR };
const GLfloat FOGDENSITY  = { 0.30f };
const GLfloat FOGSTART    = { 1.5 };
const GLfloat FOGEND      = { 4. };

// Identity matrix used to reset the rotateMatrix
float identityMatrix[16] = {
	1.0, 0.0, 0.0, 0.0,
	0.0, 1.0, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.0, 0.0, 0.0, 1.0
};

// Constants used to name GLUI buttons
#define RESET	0
#define QUIT	1

// Ugliness to deal with C++ and callbacks! GRR!
static VolumeUI *self = NULL;	// Self referential pointer (DO NOT INSTANTIATE MORE THAN ONE OF THESE OBJECTS!)
// GLUT Callbacks
static void displayGLUT()			{ if( self != NULL ) self->display(); }
static void motionGLUT(int x, int y){ if( self != NULL ) self->motion( x, y ); }
static void mouseGLUT( int button, int state, int x, int y )
									{ if( self != NULL ) self->mouse( button, state, x, y ); }
static void animateGLUT()			{ if( self != NULL ) self->animate(); }
static void reshapeGLUT(int w,int h){ if( self != NULL ) self->reshape( w, h); }
static void keyboardGLUT(unsigned char k,int m, int n){if( self != NULL ) self->keyboard( k, m ); }

// GLUI Callbacks
static void buttonGLUI( int id )	{ if( self != NULL ) self->buttons( id ); }

#pragma mark -
#pragma mark Public Interface

// Constructor
VolumeUI::VolumeUI( int argc, char **argv, Model *theModel ) {
	self = this;
	// Cross-link the model and this object
	model = theModel;
	
	// Pass the commandline parameters to GLUT, and initialize pixel formats
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowPosition( WINDOW_INIT_X, WINDOW_INIT_Y );
	glutInitWindowSize( WINDOW_INIT_SIZE_X, WINDOW_INIT_SIZE_Y );

	// Create the main graphics window
	this->mainWindow = glutCreateWindow( "Volume Render" );
	
	// Set the GLUT callbacks
	glutDisplayFunc( displayGLUT );
//	glutReshapeFunc( this->reshape );
	glutMouseFunc( mouseGLUT );
	glutMotionFunc( motionGLUT );
//	glutReshapeFunc( reshapeGLUT );
	glutKeyboardFunc( keyboardGLUT );
	// Create and initialize glui, and make the user interface
	initGlui();

	// Set some one-time GL flags
	glEnable( GL_DEPTH_TEST );
	glClearColor( 0.0, 0.0, 0.0, 1.0 );
	
	// Generate static drawing lists
	axesList = generateAxesList();

	return;
}

// Method to begin the event loops (never returns)
void VolumeUI::run() {
	reset();
	glutMainLoop();
}

#pragma mark -
#pragma mark Private initialization functions
void VolumeUI::initGlui() {
	GLint viewport[4];
	
	// Create the glui object
	this->glui = GLUI_Master.create_glui( "Parameters", 0, WINDOW_INIT_X + WINDOW_INIT_SIZE_X, WINDOW_INIT_Y );
#ifdef ANIMATE
	GLUI_Master.set_glutIdleFunc( animateGLUT );
#endif
	
	// Crate modifiers for global parameters
	GLUI_Rollout *globalParameters = new GLUI_Rollout( glui, "Global Parameters", false );
		new GLUI_Checkbox( globalParameters, "Perspective", &this->perspective );
		new GLUI_Checkbox( globalParameters, "Intensity Depth Cue", &this->intensityDepthCue );
		new GLUI_Checkbox( globalParameters, "Display Axes", &this->displayAxes );
	
	// Allow the model to load UI parameters it wants
	model->initUI( glui );
	
	// Create widgets to manipulate the modelview matrix
	GLUI_Panel *modelviewPanel = new GLUI_Panel( glui, "Modelview transformations" ); {
		// Rotation Ball
//		GLUI_Rotation *rotation = new GLUI_Rotation( modelviewPanel, "Rotation", this->rotateMatrix );
//		rotation->set_spin( 1.0 );
		
		// Scale
		new GLUI_Column( modelviewPanel, false );
		GLUI_Translation *scaleTrans = new GLUI_Translation( modelviewPanel, "Scale", GLUI_TRANSLATION_Y, &scale );
		scaleTrans->set_speed( 0.01 );
		
		// XY Translator
		new GLUI_Column( modelviewPanel, false );
		GLUI_Translation *transXY = new GLUI_Translation( modelviewPanel, "XY Translation", GLUI_TRANSLATION_XY, &transXYZ[0] );
		transXY->set_speed( 0.01 );
		
		// Z Translator
		new GLUI_Column( modelviewPanel, false );
		GLUI_Translation *transZ = new GLUI_Translation( modelviewPanel, "Z Translation", GLUI_TRANSLATION_Z, &transXYZ[2] );
		transZ->set_speed( 0.01 );
	}

	// Create the debug checkbox, reset and quit buttons
	new GLUI_Checkbox( glui, "Debug", &this->debug );

	GLUI_Panel *buttonPanel = new GLUI_Panel( glui, "" ); {
		new GLUI_Button( buttonPanel, "Reset", RESET, buttonGLUI );
		new GLUI_Column( buttonPanel, false );
		new GLUI_Button( buttonPanel, "Quit", QUIT, buttonGLUI );
	}
	
	// Do some GLUI housekeeping
	glui->set_main_gfx_window( mainWindow );
	
	// Get an arcball for screen-clicking rotation
	glGetIntegerv( GL_VIEWPORT, viewport );
	ball = new ArcBall( rotateMatrix, (int *)viewport );	
	return;
}

void VolumeUI::reset( void ) {
	// Set defaults on the boolean values
	debug = 0;
	perspective = 1;
	intensityDepthCue = 0;
	displayAxes = 0;
	
	// Set the defaults on transformations
	aspect = 1.0;
	scale = 1.0;
	transXYZ[0] = transXYZ[1] = transXYZ[2] = 0.0;
	
	for( int i = 0; i < 16; i++ )
		rotateMatrix[i] = identityMatrix[i];

	model->reset();
	
	glui->sync_live();
	glutPostRedisplay();
	
	return;
}

void VolumeUI::applyTransforms( void ) {
	glTranslatef( (GLfloat)transXYZ[0], (GLfloat)transXYZ[1], (GLfloat)transXYZ[2] );
	glMultMatrixf( (const GLfloat *)rotateMatrix );
	glScalef( (GLfloat)scale, (GLfloat)scale, (GLfloat)scale);	
}

#pragma mark -
#pragma mark Callbacks
void VolumeUI::display( void ) {
	
	glutSetWindow( mainWindow );	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	// Load the projection matrix
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	if( perspective )
		gluPerspective( 90.0, aspect, 0.1, 1000.0 );
	else
		glOrtho( -3.0*aspect, 3.0*aspect, -3.0, 3.0, 0.1, 1000.0 );
	
	// Load the modelview matrix and apply modeling transforms
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	gluLookAt( 0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	applyTransforms();
	
	// Set the fog parameters
	if( intensityDepthCue ) {
		glFogi( GL_FOG_MODE, FOGMODE );
		glFogfv( GL_FOG_COLOR, FOGCOLOR );
		glFogf( GL_FOG_DENSITY, FOGDENSITY );
		glFogf( GL_FOG_START, FOGSTART );
		glFogf( GL_FOG_END, FOGEND );
		glEnable( GL_FOG );
	} else
		glDisable( GL_FOG );
	
	if ( displayAxes ) {
		glCallList( axesList );
	}

	if( debug )
		fprintf( stderr, "Finishing-up the draw routine, about to call the model draw function\n");
	
	model->draw();
	
	glFlush();
	glutSwapBuffers();
	
	return;
}

void VolumeUI::mouse( int button, int state, int x, int y ) {
	int buttonBitField;	// Temporary value to store the button presses in a bit field
	GLint viewport[4];
	
	if( debug )
		printf( "Mouse button: %d, %d, %d, %d\n", button, state, x, y );
	
	switch( button ) {
		case GLUT_LEFT_BUTTON:
			buttonBitField = LEFT;
			break;
		case GLUT_MIDDLE_BUTTON:
			buttonBitField = MIDDLE;
			break;
		case GLUT_RIGHT_BUTTON:
			buttonBitField = RIGHT;
			break;
		default:
			buttonBitField = 0;
			printf( "Unknown button press: %d\n", button );
	}
	
	glGetIntegerv( GL_VIEWPORT, viewport );
	ball->screenResize( (int*)viewport );

	if( state == GLUT_DOWN ) {
		mouseX = x;
		mouseY = y;
		activeButton |= buttonBitField;	// Set the bit on mouse down

		if( buttonBitField == LEFT )
			ball->mouseDown( x, y );

	} else {
		if( buttonBitField == LEFT )
			ball->mouseUp();
		
		activeButton &= ~buttonBitField;// Clear the bit on mouse up
	}
	
}

void VolumeUI::motion( int x, int y ) {

	if( activeButton & LEFT )
		ball->mouseMotion( x, y );
	
//	if( activeButton & RIGHT ) {
		
//	}
		
	glutPostRedisplay();
	
	return;
}

void VolumeUI::keyboard( unsigned char k, int m ) {
	model->keyboard( k, m );
	cout << "Got a key press" << endl;
	glutPostRedisplay();
	
}
void VolumeUI::animate() {
	
	if( glutGetWindow() != mainWindow )
		glutSetWindow( mainWindow );
	
	model->animate();
	
	glutPostRedisplay();
	
}

void VolumeUI::reshape( int width, int height ) {
	GLint viewport[4];
	
//	aspect = width / height;
	
	// Revise the viewport for the arcball
//	glGetIntegerv( GL_VIEWPORT, viewport );
//	ball->screenResize( (int*)viewport );
	aspect = 1.0;
	glutPostRedisplay();

}

void VolumeUI::buttons( int id ) {
	switch (id) {
		case RESET:
			this->reset();
			break;
		case QUIT:
			exit( 0 );
			break;
		default:
			printf("I don't know how I got here...");
			break;
	}
}