/*
 *  VolumeUI.h
 *  VolumeRender
 *
 *  Created by William Dillon on 1/30/07.
 *  Copyright 2007 VIA Computing. All rights reserved.
 *
 */

#include <glui.h>
#include <GLUT/glut.h>
#include "Utility.h"

#ifndef VOLUMEUI_H
#define VOLUMEUI_H
//#include "Model.h"

class Model;

class VolumeUI {
public:
	// Constructor for volume UI argc and argv are used for glutInit( argc, argv )
	VolumeUI( int argc, char **argv, Model *theModel );
	
	// Begin event loops
	void run( void );
	
	// Function available to apply the transforms the UI manages
	void applyTransforms( void );
	float scale;			// Value used for scale in the modelview
	float transXYZ[3];		// Translation values
	float rotateMatrix[16];	// Rotation Matrix
//	float rotateX, rotateY;	// Rotation values for X and Y axes
	
	// Callbacks
	void display( void );	// Refresh the data in the display
	void buttons( int id );	// Do the action of a button click
	void mouse( int button, int state, int x, int y );
	void motion( int x, int y );	// Mouse motion
	void animate();			// Animate a scene
	void reshape(int w, int h);		// Revise the viewport size
	void keyboard(unsigned char k, int m );	// Accept key-presses
private:
	// Global parameters
	int	debug;				// Boolean for printing debugging information to the console
	int	perspective;		// Use a perspective projection matrix if true
	float aspect;			// Aspect ratio of the window
	int	intensityDepthCue;	// Use a black fog to diminish the visual impact of distant data
	int displayAxes;		// Boolean used for display of the data axes
	int mouseX, mouseY;		// Most recently know location of mouse coordinates
	int activeButton;		// Bitfield containing the currently depressed buttons
	ArcBall *ball;
	
	Model *model;			// Reference to the model object
	int mainWindow;			// Reference to the GLUT main drawing window
	GLUI *glui;
	
	// Drawing lists
	int axesList;			// Drawing list for the axes
	
	// Intialization functions
	void reset( void );		// Set all state variables to initial values
	void initGlui( void );	// Create an instance of glui and create the user interface
};
#endif
