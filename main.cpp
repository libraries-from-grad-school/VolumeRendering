/* main.cpp - Volume Render
 *
 * Framework to display volumes of scalar data
 */

#include <iostream>
#include "DopplerVolume.h"
#include "Scatter3D.h"
#include "VolumeData.h"
#include "VectorField.h"
#include "PointField.h"
#include "VolumeRender.h"
#include "TerrainVis.h"
#include "Utility.h"
#include "Keyframe.h"
#include "VolumeUI.h"

#define DATA_DENSITY 256

int main (int argc, char *argv[]) {
	Model *theModel;

//#ifdef VOLUMERENDER
	if( argc == 1 ) {
		theModel = new VolumeRender(DATA_DENSITY, DATA_DENSITY, DATA_DENSITY);		
	} else {
		printf("Using filename %s.\n", argv[1]);
		theModel = new VolumeRender( argv[1] );
	}
/*#else if VECTORFIELD
	theModel = new VectorField(DATA_DENSITY);
#else if POINTFIELD
	theModel = new PointField(DATA_DENSITY, DATA_DENSITY, DATA_DENSITY);
#else if SCATTER3D
	theModel = new Scatter3D( argv[1], atoi(argv[2]) );
#else if TERRAINVIS
	theModel = new TerrainVis( argv[1], argv[2] );
#else if KEYFRAME
	theModel = new Keyframe( DATA_DENSITY, DATA_DENSITY, DATA_DENSITY );
#else if DOPPLERVOLUME
	theModel = new DopplerVolume( argv[1] );
#else VOLUMEDATA
	theModel = new VolumeData( argv[1], atoi(argv[2]), atoi(argv[3]), atoi(argv[4]), atoi(argv[5]) );
#endif
*/
	VolumeUI *ui = new VolumeUI( argc, argv, theModel );
	theModel->setUI( ui );
	ui->run();
	
    return 0;
}
